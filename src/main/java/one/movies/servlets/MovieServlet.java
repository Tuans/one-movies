package one.movies.servlets;

import static one.movies.utils.FunctionUtils.mapActionUserPage;
import static one.movies.utils.MovieUtils.getMovieByName;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import one.movies.model.Movie;

import org.apache.log4j.Logger;

@WebServlet("/watch")
public class MovieServlet extends HttpServlet {
	private static final long serialVersionUID = -4751036228276921439L;
	private final static Logger log = Logger.getLogger(HomeServlet.class);

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public MovieServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String actionType = request.getParameter("actionType");
		System.out.println(actionType);
		try {
			Object movieServlet = MovieServlet.class.newInstance();
			HashMap<String, String> mapActionAndMethodName = mapActionUserPage();
			if (mapActionAndMethodName.containsKey(actionType)) {
				String methodName = mapActionAndMethodName.get(actionType);
				Class<?>[] paramTypes = { HttpServletRequest.class, HttpServletResponse.class };
				Method method = movieServlet.getClass().getMethod(methodName, paramTypes);
				method.invoke(this, request, response);
			}
		} catch (Exception e) {
			log.error("Error when perform action: " + actionType, e);
		}
	}

	public void addMovieToFavorite(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
			System.out.println("Perform addMovieToFavorite");

			return;
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String movieName = (String) request.getParameter("movieName");
		HttpSession session = request.getSession(false);
		if(session!=null){
			if(movieName==null || movieName.isEmpty()){
				getServletContext().getRequestDispatcher("/error-page/404.html").forward(request, response);
				return;
			}
			Movie movie = getMovieByName(movieName);
			if(movie.getMovieName()==null || movie.getMovieName().isEmpty()){
				getServletContext().getRequestDispatcher("/error-page/404.html").forward(request, response);
				return;
			}
			session.setAttribute("movie", movie);
		}
		getServletContext().getRequestDispatcher("/one-movie/watch-movie.jsp").forward(request, response);

	}

	public void init() throws ServletException {
		System.out.println("Servlet " + this.getServletName() + " has started");
	}

	public void destroy() {
		System.out.println("Servlet " + this.getServletName() + " has stopped");
	}
	
}
