package one.movies.servlets;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.servlet.GenericServlet;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.bson.Document;

import one.movies.database.MovieDatabase;
import one.movies.database.UserDatabase;
import one.movies.model.Movie;
import one.movies.model.User;

import static one.movies.utils.Constants.*;
import static one.movies.utils.FunctionUtils.*;

;

@WebServlet("/home")
public class HomeServlet extends HttpServlet {
	private static final long serialVersionUID = -4751096228264971485L;
	private final static Logger log = Logger.getLogger(HomeServlet.class);
	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public HomeServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String actionType = request.getParameter("actionType").trim();
		try {
			Object homeServlet = HomeServlet.class.newInstance();
			HashMap<String, String> mapActionAndMethodName = mapActionHomePage();
			if (mapActionAndMethodName.containsKey(actionType)) {
				String methodName = mapActionAndMethodName.get(actionType);
				Class<?>[] paramTypes = { HttpServletRequest.class, HttpServletResponse.class };
				Method method = homeServlet.getClass().getMethod(methodName, paramTypes);
				method.invoke(this, request, response);
			}
		} catch (Exception e) {
			log.error("Error when perform action: " + actionType, e);
		}
	}

	public void login(HttpServletRequest request, HttpServletResponse response) {
		try {
			String userName = request.getParameter("userName").trim();
			String password = request.getParameter("password").trim();
			if (isNullOrEmpty(userName) || isNullOrEmpty(password)) {
				request.setAttribute("loginErrorMessage", "Username and Password is require");
				doGet(request, response);
			}
			UserDatabase userDatabase = new UserDatabase();
			Document userDocument = userDatabase.getUserByName(userName);

			if (!encrypt(password).equals(userDocument.getString("Password"))) {
				request.setAttribute("loginErrorMessage", "Invalid user name or password");
				doGet(request, response);
			} else {
				HttpSession session=request.getSession(false);  
				User user = User.toUser(userDocument);
				session.setAttribute("user", user);
				doGet(request, response);
			}
		} catch (Exception e) {
			log.error("Error when perform login", e);
		}
	}
	
	public void registration(HttpServletRequest request, HttpServletResponse response) {
		try {
			String userName = request.getParameter("userName").trim();
			String displayName = request.getParameter("displayName").trim();
			String password = request.getParameter("password").trim();
			String email = request.getParameter("email").trim();
			String phoneNumber = request.getParameter("phoneNumber").trim();
			
			if (isNullOrEmpty(userName) || isNullOrEmpty(password) || isNullOrEmpty(phoneNumber)) {
				request.setAttribute("registrationErrorMessage", "Username, Password and Phone Number is require");
				doGet(request, response);
			}
			password = encrypt(password);
			User user = new User(userName, password, displayName, email, phoneNumber, "", "", "");
			UserDatabase userDatabase = new UserDatabase();
			boolean isAddOrUpdateUserSuccess = userDatabase.addOrUpdateUser(user, true);
			if(!isAddOrUpdateUserSuccess){
				request.setAttribute("registrationErrorMessage", "Error happend");
				doGet(request, response);
			}else{
				HttpSession session=request.getSession(false);  
				session.setAttribute("user", user);
				doGet(request, response);
			}
		} catch (Exception e) {
			log.error("Error when perform login", e);
		}
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession(false);
		Document filter = new Document();
		if(session!=null && session.getAttribute("user") instanceof User){
			User user = (User) session.getAttribute("user");
			if("root".equals(user.getUserName())){
				filter.append("Genres", "21+");
			}
		}
		MovieDatabase movieDatabase = new MovieDatabase();
		List<Movie> lstMovie = movieDatabase.getListMovie(filter);
		request.setAttribute("lstMovie", lstMovie);
		if(session!=null){
			session.setAttribute("currentPage", HOME_PAGE);
		}
		getServletContext().getRequestDispatcher("/one-movie/home.jsp").forward(request, response);
	}

	public void init() throws ServletException {
		super.init();
		System.out.println("Servlet " + this.getServletName() + " has started");
	}

	public void destroy() {
		super.destroy();
		System.out.println("Servlet " + this.getServletName() + " has stopped");
	}
	public static void main(String[] args) {
		System.out.println(new Date(1487194729));
	}
}
