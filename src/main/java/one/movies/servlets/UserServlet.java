package one.movies.servlets;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import one.movies.database.UserDatabase;
import one.movies.model.User;

import static one.movies.utils.Constants.*;
import static one.movies.utils.FunctionUtils.encrypt;
import static one.movies.utils.FunctionUtils.isNullOrEmpty;
import static one.movies.utils.FunctionUtils.mapActionUserPage;

@WebServlet("/user")
public class UserServlet extends HttpServlet {
	private static final long serialVersionUID = -4751036228276921485L;
	private final static Logger log = Logger.getLogger(HomeServlet.class);

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String actionType = request.getParameter("actionType");
		System.out.println(actionType);
		try {
			Object userServlet = UserServlet.class.newInstance();
			HashMap<String, String> mapActionAndMethodName = mapActionUserPage();
			if (mapActionAndMethodName.containsKey(actionType)) {
				String methodName = mapActionAndMethodName.get(actionType);
				Class<?>[] paramTypes = { HttpServletRequest.class, HttpServletResponse.class };
				Method method = userServlet.getClass().getMethod(methodName, paramTypes);
				method.invoke(this, request, response);
			}
		} catch (Exception e) {
			log.error("Error when perform action: " + actionType, e);
		}
	}

	public void getUserProfile(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
			System.out.println("Perform getUserProfile");
			HttpSession session = request.getSession(false);
			session.setAttribute("currentPage", USER_PROFILE_PAGE);
			doGet(request, response);
			return;
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void logout(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			System.out.println("Perform logout");
			HttpSession session = request.getSession(false);
			if (session.getAttribute("user") instanceof User) {
				session.removeAttribute("user");
			}
			getServletContext().getRequestDispatcher("/index.jsp").forward(request, response);
			return;
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void updateUserProfile(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("Perform updateUserProfile");
		try {
			String userName = request.getParameter("userName").trim();
			String displayName = request.getParameter("displayName").trim();
			String password = request.getParameter("password").trim();
			String email = request.getParameter("email").trim();
			String phoneNumber = request.getParameter("phoneNumber").trim();
			String dateOfBirth = request.getParameter("dateOfBirth").trim();
			String country = request.getParameter("country").trim();
			String userDescription = request.getParameter("aboutYourSelf").trim();
			User user = new User(userName, password, displayName, email, phoneNumber, dateOfBirth, country,
					userDescription);
			if (isNullOrEmpty(userName) || isNullOrEmpty(phoneNumber)) {
				HttpSession session = request.getSession(false);
				session.setAttribute("user", user);
				request.setAttribute("updateUserProfileErrorMessage", "Username and Phone Number is require");
				doGet(request, response);
				return;
			}
			password = encrypt(password);
			user.setPassword(password);
			UserDatabase userDatabase = new UserDatabase();
			boolean isAddOrUpdateUserSuccess = userDatabase.addOrUpdateUser(user, false);
			if (!isAddOrUpdateUserSuccess) {
				request.setAttribute("updateUserProfileErrorMessage", "Error happend");
				doGet(request, response);
			} else {
				request.setAttribute("updateUserProfileSuccessMessage", "Updated your profile");
				HttpSession session = request.getSession(false);
				session.setAttribute("user", user);
				doGet(request, response);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession(false);
		if (session!=null && session.getAttribute("user") instanceof User) {
			getServletContext().getRequestDispatcher("/one-movie/user-profile.jsp").forward(request, response);
		}else{
			getServletContext().getRequestDispatcher("/index.jsp").forward(request, response);
		}
	}

	public void init() throws ServletException {
		System.out.println("Servlet " + this.getServletName() + " has started");
	}

	public void destroy() {
		System.out.println("Servlet " + this.getServletName() + " has stopped");
	}

}
