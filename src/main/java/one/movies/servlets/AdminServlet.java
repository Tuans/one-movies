package one.movies.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.bson.Document;

import one.movies.database.MovieDatabase;
import one.movies.model.Movie;
import one.movies.model.User;
import static one.movies.utils.FunctionUtils.isNullOrEmpty;

/**
 * @author TuanNA34
 *
 */
@WebServlet("/admin")
public class AdminServlet extends HttpServlet {
	private static final long serialVersionUID = -4751096228274971485L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public AdminServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String movieName = request.getParameter("movieName").trim();
		String director = request.getParameter("director").trim();
		String studio = request.getParameter("studio").trim();
		String genres = request.getParameter("genres").trim();
		String tags = request.getParameter("tags").trim();
		String stars = request.getParameter("stars").trim();
		String movieLink = request.getParameter("movieLink").trim();
		String largeImageLink = request.getParameter("largeImageLink").trim();
		String smallImageLink = request.getParameter("smallImageLink").trim();
		String releaseYear = request.getParameter("releaseYear").trim();
		String movieDescription = request.getParameter("movieDescription").trim();
		String oldSite = request.getParameter("oldSite").trim();
		String country = request.getParameter("country").trim();
		checkValidMovie(movieName, request, response);

		Movie movie = new Movie(movieName, director, studio, genres, tags, stars, movieLink, largeImageLink,
				smallImageLink, releaseYear, movieDescription, oldSite, country, "");
		System.out.println(movie);
		MovieDatabase movieDatabase = new MovieDatabase();
		boolean isSuccess = movieDatabase.addMovie(movie);
		if (isSuccess) {
			request.setAttribute("Message", "Insert movie successful");
			getServletContext().getRequestDispatcher("/admin/addMovie.jsp").forward(request, response);
		}
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession(false);
		if(session!=null && session.getAttribute("user") instanceof User){
			User user = (User) session.getAttribute("user");
			if("root".equals(user.getUserName())){
				getServletContext().getRequestDispatcher("/admin/addMovie.jsp").forward(request, response);
				return;
			}
		}
		getServletContext().getRequestDispatcher("/index.jsp").forward(request, response);
	}

	public void init() throws ServletException {
		System.out.println("Servlet " + this.getServletName() + " has started");
	}

	public void destroy() {
		System.out.println("Servlet " + this.getServletName() + " has stopped");
	}

	public void checkValidMovie(String movieName, HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		if (isNullOrEmpty(movieName)) {
			request.setAttribute("ErrorMessage", "Movie name is require");
			getServletContext().getRequestDispatcher("/admin/addMovie.jsp").forward(request, response);
		}
	}
}