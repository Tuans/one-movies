package one.movies.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.bson.Document;

/**
 * @author TuanNA34
 *
 */
public class Movie {
	private String movieName;
	private String director;
	private String studio;
	private String genres;
	private String tags;
	private String stars;
	private String movieLink;
	private String largeImageLink;
	private String oldSite;
	private String country;
	private String correctedName;

	/**
	 * @return the movieName
	 */
	public String getMovieName() {
		return movieName;
	}

	public Movie(String movieName, String director, String studio,
			String genres, String tags, String stars, String movieLink,
			String largeImageLink, String smallImageLink, String releaseYear,
			String movieDescription, String oldSite, String country,
			String correctedName) {
		super();
		this.movieName = movieName;
		this.director = director;
		this.studio = studio;
		this.genres = genres;
		this.tags = tags;
		this.stars = stars;
		this.movieLink = movieLink;
		this.largeImageLink = largeImageLink;
		this.smallImageLink = smallImageLink;
		this.releaseYear = releaseYear;
		this.movieDescription = movieDescription;
		this.oldSite = oldSite;
		this.country = country;
		this.setCorrectedName(correctedName);
	}

	/**
	 * @param movieName
	 *            the movieName to set
	 */
	public void setMovieName(String movieName) {
		this.movieName = movieName;
	}

	/**
	 * @return the director
	 */
	public String getDirector() {
		return director;
	}

	/**
	 * @param director
	 *            the director to set
	 */
	public void setDirector(String director) {
		this.director = director;
	}

	/**
	 * @return the studio
	 */
	public String getStudio() {
		return studio;
	}

	/**
	 * @param studio
	 *            the studio to set
	 */
	public void setStudio(String studio) {
		this.studio = studio;
	}

	/**
	 * @return the genres
	 */
	public String getGenres() {
		return genres;
	}

	/**
	 * @param genres
	 *            the genres to set
	 */
	public void setGenres(String genres) {
		this.genres = genres;
	}

	/**
	 * @return the tags
	 */
	public String getTags() {
		return tags;
	}

	/**
	 * @param tags
	 *            the tags to set
	 */
	public void setTags(String tags) {
		this.tags = tags;
	}

	/**
	 * @return the stars
	 */
	public String getStars() {
		return stars;
	}

	/**
	 * @param stars
	 *            the stars to set
	 */
	public void setStars(String stars) {
		this.stars = stars;
	}

	/**
	 * @return the movieLink
	 */
	public String getMovieLink() {
		return movieLink;
	}

	/**
	 * @param movieLink
	 *            the movieLink to set
	 */
	public void setMovieLink(String movieLink) {
		this.movieLink = movieLink;
	}

	/**
	 * @return the largeImageLink
	 */
	public String getLargeImageLink() {
		return largeImageLink;
	}

	/**
	 * @param largeImageLink
	 *            the largeImageLink to set
	 */
	public void setLargeImageLink(String largeImageLink) {
		this.largeImageLink = largeImageLink;
	}

	/**
	 * @return the smallImageLink
	 */
	public String getSmallImageLink() {
		return smallImageLink;
	}

	/**
	 * @param smallImageLink
	 *            the smallImageLink to set
	 */
	public void setSmallImageLink(String smallImageLink) {
		this.smallImageLink = smallImageLink;
	}

	/**
	 * @return the releaseYear
	 */
	public String getReleaseYear() {
		return releaseYear;
	}

	/**
	 * @param releaseYear
	 *            the releaseYear to set
	 */
	public void setReleaseYear(String releaseYear) {
		this.releaseYear = releaseYear;
	}

	/**
	 * @return the movieDescription
	 */
	public String getMovieDescription() {
		return movieDescription;
	}

	/**
	 * @param movieDescription
	 *            the movieDescription to set
	 */
	public void setMovieDescription(String movieDescription) {
		this.movieDescription = movieDescription;
	}

	/**
	 * @return the country
	 */
	public String getCountry() {
		return country;
	}

	/**
	 * @param country
	 *            the country to set
	 */
	public void setCountry(String country) {
		this.country = country;
	}

	private String smallImageLink;
	private String releaseYear;
	private String movieDescription;

	/**
	 * @return the oldSite
	 */
	public String getOldSite() {
		return oldSite;
	}

	/**
	 * @param oldSite
	 *            the oldSite to set
	 */
	public void setOldSite(String oldSite) {
		this.oldSite = oldSite;
	}

	/**
	 * @return the correctedName
	 */
	public String getCorrectedName() {
		return correctedName;
	}

	/**
	 * @param correctedName
	 *            the correctedName to set
	 */
	public void setCorrectedName(String correctedName) {
		this.correctedName = correctedName;
	}

	@Override
	public String toString() {
		return toDocument().toJson();
	}

	public Document toDocument() {
		Document movie = new Document("MovieName", movieName)
				.append("Director", director).append("Studio", studio)
				.append("Genres", genres).append("Tags", tags)
				.append("Stars", stars).append("MovieLink", movieLink)
				.append("LargeImageLink", largeImageLink)
				.append("SmallImageLink", smallImageLink)
				.append("ReleaseYear", releaseYear)
				.append("MovieDescription", movieDescription)
				.append("OldSite", oldSite).append("Country", country)
				.append("CorrectedName", correctedName);
		return movie;
	}

	public Movie() {
		super();
	}

	public static List<Movie> toMovie(List<Document> lstDocumentMovie) {
		List<Movie> lstMovie = new ArrayList<Movie>();
		for (Document movieDocument : lstDocumentMovie) {
			Movie movie = toMovie(movieDocument);
			lstMovie.add(movie);
		}
		return lstMovie;
	}

	public List<String> ToList(String name) {
		List<String> lst = new ArrayList<>();
		if ("Genres".equalsIgnoreCase(name) && genres!=null) {
			lst = Arrays.asList(genres.split(","));
		} else if ("Stars".equalsIgnoreCase(name) && stars!=null) {
			lst = Arrays.asList(stars.split(","));
		} else if ("Tags".equalsIgnoreCase(name) && tags!=null) {
			lst = Arrays.asList(tags.split(","));
		}
		return lst;
	}

	public static Movie toMovie(Document movieDocument) {
		Movie movie = new Movie();
		movie.setDirector(movieDocument.getString("Director"));
		movie.setGenres(movieDocument.getString("Genres"));
		movie.setLargeImageLink(movieDocument.getString("LargeImageLink"));
		movie.setSmallImageLink(movieDocument.getString("SmallImageLink"));
		movie.setMovieDescription(movieDocument.getString("MovieDescription"));
		movie.setMovieLink(movieDocument.getString("MovieLink"));
		movie.setMovieName(movieDocument.getString("MovieName"));
		movie.setReleaseYear(movieDocument.getString("ReleaseYear"));
		movie.setStars(movieDocument.getString("Stars"));
		movie.setStudio(movieDocument.getString("Studio"));
		movie.setTags(movieDocument.getString("Tags"));
		movie.setOldSite(movieDocument.getString("OldSite"));
		movie.setCountry(movieDocument.getString("Country"));
		movie.setCorrectedName(movieDocument.getString("CorrectedName"));
		return movie;
	}
}