package one.movies.model;

import org.bson.Document;

public class User {
	private String userName;
	private String password;
	private String displayName;
	private String email;
	private String phoneNumber;
	private String dateOfBirth;
	private String country;
	private String userDescription;
	
	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * @param userName the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return the displayName
	 */
	public String getDisplayName() {
		return displayName;
	}

	/**
	 * @param displayName the displayName to set
	 */
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the phoneNumber
	 */
	public String getPhoneNumber() {
		return phoneNumber;
	}

	/**
	 * @param phoneNumber the phoneNumber to set
	 */
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	/**
	 * @return the dateOfBirth
	 */
	public String getDateOfBirth() {
		return dateOfBirth;
	}

	/**
	 * @param dateOfBirth the dateOfBirth to set
	 */
	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	/**
	 * @return the country
	 */
	public String getCountry() {
		return country;
	}

	/**
	 * @param country the country to set
	 */
	public void setCountry(String country) {
		this.country = country;
	}

	/**
	 * @return the userDescription
	 */
	public String getUserDescription() {
		return userDescription;
	}

	/**
	 * @param userDescription the userDescription to set
	 */
	public void setUserDescription(String userDescription) {
		this.userDescription = userDescription;
	}

	public User(String userName, String password, String displayName, String email, String phoneNumber,
			String dateOfBirth, String country, String userDescription) {
		super();
		this.userName = userName;
		this.password = password;
		this.displayName = displayName;
		this.email = email;
		this.phoneNumber = phoneNumber;
		this.dateOfBirth = dateOfBirth;
		this.country = country;
		this.userDescription = userDescription;
	}

	public User() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return toDocument().toJson();
	}
	
	public Document toDocument(){
		Document user = new Document("UserName",userName)
				.append("DisplayName", displayName)
				.append("Password", password)
				.append("Email", email)
				.append("PhoneNumber", phoneNumber)
				.append("DateOfBirth", dateOfBirth)
				.append("Country", country)
				.append("UserDescription", userDescription);
		return user;
	}
	
	public static User toUser(Document userDocument){
		User user = new User();
		user.setCountry(userDocument.getString("Country"));
		user.setDateOfBirth(userDocument.getString("DateOfBirth"));
		user.setDisplayName(userDocument.getString("DisplayName"));
		user.setEmail(userDocument.getString("Email"));
		user.setPassword(userDocument.getString("Password"));
		user.setPhoneNumber(userDocument.getString("PhoneNumber"));
		user.setUserDescription(userDocument.getString("UserDescription"));
		user.setUserName(userDocument.getString("UserName"));
		return user;
	}
}
