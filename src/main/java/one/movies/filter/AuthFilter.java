package one.movies.filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import one.movies.utils.ConfigurationUtils;
import one.movies.utils.Constants;

/**
 * @author TuanNA34
 *
 */
@WebFilter("/*")
public class AuthFilter implements Filter {
	private List<String> whiteURLs;

	public void init(FilterConfig filterConfig) throws ServletException {
		// Hard code for white list URLs
		whiteURLs = new ArrayList<String>();
		whiteURLs.add("/admin");
		whiteURLs.add("/home");
		whiteURLs.add("/watch");
		whiteURLs.add("/user");
	}

	public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain)
			throws IOException, ServletException {
		if(checkMaintain(req, resp)){
			return;
		}
		req.setCharacterEncoding("UTF-8");
		HttpServletRequest request = (HttpServletRequest) req;
		String path = request.getRequestURI().substring(request.getContextPath().length());
		if (checkWhiteURL(path)) {
			chain.doFilter(req, resp);
		} else {
			HttpSession session = request.getSession();
			String userInfo = (String) session.getAttribute("username");
			if (userInfo != null) {
				chain.doFilter(req, resp);
			} else {
				RequestDispatcher rd = request.getRequestDispatcher("/error-page/404.html");
				rd.forward(req, resp);
			}
		}
	}

	private boolean checkMaintain(ServletRequest req, ServletResponse resp) throws ServletException, IOException {
		boolean isUnderMaintain = Boolean.parseBoolean(ConfigurationUtils.getProperties().getProperty("isUnderMaintain"));
		if(isUnderMaintain){
			HttpServletRequest request = (HttpServletRequest) req;
			RequestDispatcher rd = request.getServletContext().getRequestDispatcher("/error-page/maintenance.html");
			rd.forward(req, resp);
			return true;
		}
		return false;
	}

	public void destroy() {
	}

	private boolean checkWhiteURL(String path) {
		boolean isWhiteURL = false;
		for (String whiteURL : whiteURLs) {
			isWhiteURL = path.isEmpty() || "/".equals(path) || path.contains(whiteURL) ? true : false;
			if (isWhiteURL)
				break;
		}
		return isWhiteURL;
	}
}