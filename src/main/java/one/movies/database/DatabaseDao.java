package one.movies.database;


import org.bson.Document;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

/**
 * 
 * @author TuanNA34
 *
 */
public class DatabaseDao {

	private MongoDatabase database;

	public DatabaseDao(final MongoDatabase database) {
		super();
		this.database = database;
	}

	public DatabaseDao() {
		super();
	}

	public MongoCollection<Document> getCollection(final String collectionName) {
		return database.getCollection(collectionName);
	}
}
