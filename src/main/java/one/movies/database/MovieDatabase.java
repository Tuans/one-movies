package one.movies.database;

/**
 * @author Tuans
 */
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import one.movies.model.Movie;
import one.movies.utils.FunctionUtils;

import org.apache.log4j.Logger;
import org.bson.Document;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriverService;
import org.openqa.selenium.remote.DesiredCapabilities;

import com.mongodb.bulk.BulkWriteResult;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.UpdateOneModel;
import com.mongodb.client.model.UpdateOptions;
import com.mongodb.client.model.WriteModel;
import com.mongodb.client.result.UpdateResult;

import static one.movies.utils.FunctionUtils.*;


public class MovieDatabase {
	public final static int RANGE_OF_RE_NEW_LINK = 15;
	public final static String MOVIE_COLLECTION = "movies";
	private final static Logger log = Logger.getLogger(MovieDatabase.class);

	private MongoCollection<Document> movieCollection;

	private void getCollection() {
		if (movieCollection == null) {
			movieCollection = CollectionDao.getCollection(MOVIE_COLLECTION);
		}
	}

	public boolean addMovie(Movie movie) {
		getCollection();
		if (movieCollection == null) {
			log.error("addMovie: Cannot initialize the database");
			System.err.println("addMovie: Cannot initialize the database");
			return false;
		}
		log.debug("New movie value: " + movie.toString());
		try {
			Document movieDocument = movie.toDocument();
			Date dateNow = new Date();
			movieDocument.append("CreatedTime", dateNow)
					.append("LastUpdatedTime", dateNow).append("Viewed", 0);

			String movieName = movie.getMovieName();
			if (isNullOrEmpty(movieName)) {
				System.err.println("addMovie: invalid movie: "
						+ movie.toString());
				log.error("addMovie: invalid movie: " + movie.toString());
				return false;
			}
			UpdateResult updateResult = movieCollection.updateOne(new Document(
					"MovieName", movieName),
					new Document("$set", movieDocument), new UpdateOptions()
							.upsert(true));
			System.out.println("MatchedCount: "
					+ updateResult.getMatchedCount() + " | ModifiedCount: "
					+ updateResult.getModifiedCount() + " | UpsertedId: "
					+ updateResult.getUpsertedId());
			log.debug("MatchedCount: " + updateResult.getMatchedCount()
					+ " | ModifiedCount: " + updateResult.getModifiedCount()
					+ " | UpsertedId: " + updateResult.getUpsertedId());
			return true;
		} catch (Exception e) {
			log.error(
					"Error when add movie to database, movie: "
							+ movie.toString(), e);
			return false;
		}
	}

	public Document getMovieByCorrectedName(String correctedName) {
		getCollection();
		if (movieCollection == null) {
			log.error("getMovieByCorrectedName: Cannot initialize the database");
			System.err
					.println("getMovieByCorrectedName: Cannot initialize the database");
			return new Document();
		}
		log.debug("getMovieByCorrectedName: correctedName: " + correctedName);
		try {
			List<Document> lstMovie = movieCollection.find(
					new Document("CorrectedName", correctedName)).into(
					new ArrayList<Document>());
			if (lstMovie.size() == 0 || lstMovie.size() > 1) {
				log.error("Error when get movie by correctedName: " + correctedName
						+ ", number of movie return: " + lstMovie.size());
				System.err.println("Error when get movie by correctedName: " + correctedName
						+ ", number of movie return: " + lstMovie.size());
			}
			return lstMovie.get(0);
		} catch (Exception e) {
			log.error("Error when get movie by correctedName: " + correctedName, e);
			return new Document();
		}
	}

	public List<Movie> getListMovie(Document filter) {
		getCollection();
		if (movieCollection == null) {
			log.error("getListMovie: Cannot initialize the database");
			System.err
					.println("getListMovie: Cannot initialize the database");
			return new ArrayList<Movie>();
		}
		if(!filter.containsKey("Genres")){
			filter.append("Genres", new Document("$ne", "21+"));
		}
		int skip = -1;
		int limit =-1;
		if(filter.containsKey("Skip")){
			skip = filter.getInteger("Skip", 0);
		}
		if(filter.containsKey("Limit")){
			limit = filter.getInteger("Limit", 0);
		}
		log.debug("getListMovie: filter: " + filter);
		List<Movie> lstMovie = new ArrayList<Movie>();
		try {
			FindIterable<Document> findIterable = movieCollection.find(filter);
			if(limit>-1){
				findIterable.limit(limit);
			}
			if(skip>-1){
				findIterable.skip(skip);
			}
			List<Document> lstDocumentMovie = findIterable.into(new ArrayList<Document>());
			lstMovie = Movie.toMovie(lstDocumentMovie);
		} catch (Exception e) {
			log.error("getListMovie: Error with filter: " + filter.toJson(),e);
			e.printStackTrace();
		}
		return lstMovie;
	}
	

	public List<String> getListMovieName(Document filter) {
		getCollection();
		if (movieCollection == null) {
			log.error("getListMovieName: Cannot initialize the database");
			System.err
					.println("getListMovieName: Cannot initialize the database");
			return new ArrayList<String>();
		}
		if(!filter.containsKey("Genres")){
			filter.append("Genres", new Document("$ne", "21+"));
		}
		log.debug("getListMovieName: filter: " + filter);
		List<String> lstMoviename = new ArrayList<String>();
		try {
			List<Document> lstDocumentMovie = movieCollection.find(filter)
					.into(new ArrayList<Document>());
			for (Document movieDocument : lstDocumentMovie) {
				lstMoviename.add(movieDocument.getString("MovieName"));
			}
		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		}
		return lstMoviename;
	}
	
	public void reNewLink(int skip, int limit) {
		getCollection();
		try {
			List<Document> lstMovie = movieCollection.find(
					new Document("Genres", "21+")).skip(skip).limit(limit).into(
					new ArrayList<Document>());
			DesiredCapabilities caps = new DesiredCapabilities();
			caps.setJavascriptEnabled(true);
			caps.setCapability(PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY,
					"D:\\Tools\\phantomjs-2.1.1-windows\\bin\\phantomjs.exe");
			WebDriver driver = new PhantomJSDriver(caps);
			List<WriteModel<Document>> bulkWriteList = new ArrayList<WriteModel<Document>>();
			
			for (Document movie : lstMovie) {
				String url = movie.getString("OldSite");
				System.out.println(url);
				if(url!=null && !url.isEmpty()){
					String newMovieLink = FunctionUtils.getLink(driver, url);
					if(newMovieLink!=null && !newMovieLink.equals(movie.getString("MovieLink"))){
						bulkWriteList.add(new UpdateOneModel<Document>(new Document("OldSite", url), new Document("$set", new Document("MovieLink", newMovieLink).append("LastUpdatedTime", new Date()))));
					}
					if(driver!=null){
						driver.quit();
					}
				}
			}
			
			BulkWriteResult bulkWriteResult = movieCollection.bulkWrite(bulkWriteList);
			String message = String.format("Updated successful %d matched, %d inserted, %d modified", bulkWriteResult.getMatchedCount(), bulkWriteResult.getInsertedCount(), bulkWriteResult.getModifiedCount());
			System.out.println(message);
			log.info(message);
		} catch (Exception e) {
			log.error("Error when renew movie link" , e);
		}
	}
	
	public static void main(String[] args) throws IOException, InterruptedException {
		//RenewMovieLink();
		String url = "http://watch8x.com/Watch_GDHH008-Sister-And-Shared-Room-In-A-Long-Time-Family-Trip!If-You-Think-Unable-T_261615.aspx";
		DesiredCapabilities caps = new DesiredCapabilities();
		caps.setJavascriptEnabled(true);
		caps.setCapability(PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY,
				"D:\\Tools\\phantomjs-2.1.1-windows\\bin\\phantomjs.exe");
		WebDriver driver = new PhantomJSDriver(caps);
		String newMovieLink = FunctionUtils.getLink(driver, url);
		System.out.println(newMovieLink);
	}
}
