package one.movies.database;

import static one.movies.utils.FunctionUtils.isNullOrEmpty;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.bson.Document;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.UpdateOptions;
import com.mongodb.client.result.UpdateResult;

import one.movies.model.User;

public class UserDatabase {
	public final static String USER_COLLECTION = "users";
	private final static Logger log = Logger.getLogger(UserDatabase.class);

	private MongoCollection<Document> userCollection;

	private void getCollection() {
		if (userCollection == null) {
			userCollection = CollectionDao.getCollection(USER_COLLECTION);
		}
	}

	public boolean addOrUpdateUser(User user, boolean needCheckDuplicate) {
		getCollection();
		if (userCollection == null) {
			log.error("addUser: Cannot initialize the database");
			System.err.println("addUser: Cannot initialize the database");
			return false;
		}
		log.debug("New user value: " + user.toString());
		try {
			Document userDocument = user.toDocument();
			Date dateNow = new Date();
			if(needCheckDuplicate){
				userDocument.append("CreatedTime", dateNow);
			}
			userDocument.append("LastUpdatedTime", dateNow);
			
			String userName = user.getUserName();
			String phoneNumber = user.getPhoneNumber();
			if (isNullOrEmpty(userName) || isNullOrEmpty(phoneNumber)) {
				System.err.println("addUser: invalid user: " + user.toString());
				log.error("addUser: invalid user: " + user.toString());
				return false;
			}
			if(needCheckDuplicate && !checkExistsUser(user)){
				return false;
			}
			UpdateResult updateResult = userCollection.updateOne(
					new Document("UserName", userName).append("PhoneNumber", phoneNumber),
					new Document("$set", userDocument), new UpdateOptions().upsert(true));
			System.out.println("MatchedCount: " + updateResult.getMatchedCount() + " | ModifiedCount: "
					+ updateResult.getModifiedCount() + " | UpsertedId: " + updateResult.getUpsertedId());
			log.debug("MatchedCount: " + updateResult.getMatchedCount() + " | ModifiedCount: "
					+ updateResult.getModifiedCount() + " | UpsertedId: " + updateResult.getUpsertedId());
			return true;
		} catch (Exception e) {
			log.error("Error when add user to database, user: " + user.toString(), e);
			return false;
		}
	}

	public Document getUserByName(String userName) {
		getCollection();
		if (userCollection == null) {
			log.error("getUserByName: Cannot initialize the database");
			System.err.println("getUserByName: Cannot initialize the database");
			return new Document();
		}
		log.debug("getUserByName: userName: " + userName);
		try {
			List<Document> lstUser = userCollection.find(new Document("UserName", userName))
					.into(new ArrayList<Document>());
			if (lstUser.size() == 0) {
				log.error("Not found user with name: " + userName);
				System.err.println("Not found user with name: " + userName);
				return new Document();
			}
			return lstUser.get(0);
		} catch (Exception e) {
			log.error("Error when get user by name: " + userName, e);
			return new Document();
		}
	}

	public boolean checkExistsUser(User user) {
		getCollection();
		if (userCollection == null) {
			log.error("checkExistsUser: Cannot initialize the database");
			System.err.println("checkExistsUser: Cannot initialize the database");
			return false;
		}
		log.debug("checkExistsUser: user: " + user.toString());
		try {
			List<Document> lstUser = userCollection
					.find(new Document("UserName", user.getUserName()).append("PhoneNumber", user.getPhoneNumber()))
					.into(new ArrayList<Document>());
			if (lstUser.size() > 0) {
				log.debug(
						"Existsed user with name: " + user.getUserName() + " -phone number: " + user.getPhoneNumber());
				System.err.println(
						"Existsed user with name: " + user.getUserName() + " -phone number: " + user.getPhoneNumber());
				return false;
			}
			return true;
		} catch (Exception e) {
			log.error("Error when check exists user: " + user.toString(), e);
			return false;
		}
	}
}
