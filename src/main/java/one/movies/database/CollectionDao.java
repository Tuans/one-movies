package one.movies.database;

import org.bson.Document;

import com.mongodb.client.MongoCollection;

import one.movies.utils.ConfigurationUtils;
import one.movies.utils.Constants;
import one.movies.utils.DatabaseUtils;
/**
 *
 * @author TuanNA34
 *
 */
public class CollectionDao {
	private static DatabaseDao databaseDao;

	public static MongoCollection<Document> getCollection(final String collectionName) {
		if (databaseDao == null) {
			databaseDao = new DatabaseDao(
					DatabaseUtils.loadDatabase(ConfigurationUtils.getProperties().getProperty(Constants.MONGODB_DATABASE)));
		}
		return databaseDao.getCollection(collectionName);
	}
}
