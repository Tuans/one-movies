package one.movies.utils;

import static one.movies.utils.FunctionUtils.isValidUrl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import one.movies.database.MovieDatabase;
import one.movies.model.Movie;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SyncMoviePhimMoi extends BasePage {

	public SyncMoviePhimMoi(WebDriver driver, WebDriverWait driverWait) {
		super(driver, driverWait);
		// TODO Auto-generated constructor stub
	}
	
	private String getSmallImg(String largeImg){
		return largeImg.replace("medium", "thumb");
	}

	private String getLargeImg(String src) {
		if (src == null || src.isEmpty() || !src.contains("&url=")
				|| !src.contains("%3F_iv")) {
			return "";
		}
		System.out.println(src);
		return src.substring(src.indexOf("&url=") + 5, src.indexOf("%3F_iv"));
	}
	
	private String correctMovieName(String movieName){
		return movieName.replaceAll("[^a-zA-Z0-9.-]", "_");
	}

	private Movie getMovieInformation(){
		String movieName = myGetText(By.xpath(Constants.XPATH_MOVIE_TITLE));
		String movieCountry = myGetText(By.xpath(Constants.XPATH_MOVIE_COUNTRY));
		String movieYear = myGetText(By.xpath(Constants.XPATH_MOVIE_YEAR));
		String movieLargeImg = getLargeImg(waitUtilVisible(By.xpath(Constants.XPATH_MOVIE_IMAGE)).getAttribute("src"));
		String movieSmallImg = getSmallImg(movieLargeImg);
		String movieGenres = myGetText(By.xpath(Constants.XPATH_MOVIE_GENRES));
		String movieDescription = myGetText(By.xpath(Constants.XPATH_MOVIE_DESCRIPTION));
		String movieCorrectedName = FunctionUtils.removeAccent(movieName);
		
		Movie movie = new Movie();
		movie.setMovieName(movieName);
		movie.setCountry(movieCountry);
		movie.setReleaseYear(movieYear);
		movie.setLargeImageLink(movieLargeImg);
		movie.setSmallImageLink(movieSmallImg);
		movie.setGenres(movieGenres);
		movie.setMovieDescription(movieDescription);
		movie.setCorrectedName(movieCorrectedName);
		
		return movie;
	}

	private String getMovieLink() {
		By sourceVideoLocator = By
				.xpath(".//div[@class='jw-media jw-reset']/video");
		try {
			FunctionUtils.waitUntilVisible(driver, sourceVideoLocator);
			WebElement source = driver.findElement(sourceVideoLocator);
			String movieLink = source.getAttribute("src");
			return movieLink;
		} catch (Exception e) {
			System.out.println("getMovieLink: Error, sourceVideoLocator: "
					+ sourceVideoLocator);
			e.printStackTrace();
			return "";
		}
	}

	public Movie getMovie(String url) {
		try {
			if(!isValidUrl(url)) return null;
			driver.get(url);
			//click to "xem phim" button
			Movie movie = getMovieInformation();
			myClick(By.xpath(Constants.XPATH_WATCH_MOVIE_BUTTON));
			movie.setMovieLink(getMovieLink());
			return movie;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

	}
	//TODO convert List WebElament to list String
	public List<String> toListUrl(List<WebElement> listWebElement){
		List<String> listUrl = new ArrayList<String>();
		for (WebElement movieItem : listWebElement) {
			listUrl.add(movieItem.getAttribute("href"));
		}
		return listUrl;
	}
	
	public void syncFromList(String listUrl){
		initial();
		driver.get(listUrl);
		By movieContainer = By.xpath(Constants.XPATH_MOVIE_CONTAINER);
		waitUtilVisible(movieContainer);
		List<WebElement> movieItems = driver.findElements(movieContainer);
		System.out.println("Size of list film: " + movieItems!=null?movieItems.size():0);
		MovieDatabase movieDatabase = new MovieDatabase();
		for (String url : toListUrl(movieItems)) {
			Movie movie = getMovie(url);
			System.out.println(movie);
			movieDatabase.addMovie(movie);
		}
		destroy();
	}

	public static void main(String[] args) throws IOException,
			InterruptedException {
		String listSingleFilm = "http://www.phimmoi.net/phim-le/";
		SyncMoviePhimMoi syncMoviePhimMoi = new SyncMoviePhimMoi(driver,
				driverWait);
		syncMoviePhimMoi.syncFromList(listSingleFilm);
		
	}
}
