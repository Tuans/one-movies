package one.movies.utils;

import static one.movies.utils.Constants.ACTION_ADD_TO_FAVORITE;
import static one.movies.utils.Constants.ACTION_GET_USER_PROFILE;
import static one.movies.utils.Constants.ACTION_LOGIN;
import static one.movies.utils.Constants.ACTION_LOGOUT;
import static one.movies.utils.Constants.ACTION_REGISTRATION;
import static one.movies.utils.Constants.ACTION_SEARCH;
import static one.movies.utils.Constants.ACTION_UPDATE_USER_PROFILE;

import java.io.IOException;
import java.math.BigInteger;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.URL;
import java.net.URLConnection;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.Normalizer;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class FunctionUtils {
	public static final String SECRET_KEY = "On3-M0vle";
	private final static Logger log = Logger.getLogger(FunctionUtils.class);

	public static boolean isNullOrEmpty(String str) {
		return str == null || str.trim().isEmpty() ? true : false;
	}

	/**
	 * function encrypt password with MD5
	 * 
	 * @param strPassword
	 * @return
	 */
	public static String encrypt(String strPassword) {
		String md5 = null;
		if (null == strPassword)
			return null;
		try {
			// Create MessageDigest object for MD5
			strPassword = strPassword + SECRET_KEY;
			MessageDigest digest = MessageDigest.getInstance("MD5");
			// Update input string in message digest
			digest.update(strPassword.getBytes(), 0, strPassword.length());
			// Converts message digest value in base 16 (hex)
			md5 = new BigInteger(1, digest.digest()).toString(16);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return md5;
	}

	public static HashMap<String, String> mapActionHomePage() {
		HashMap<String, String> mapActionAndMethodName = new HashMap<>();
		mapActionAndMethodName.put(ACTION_LOGIN, "login");
		mapActionAndMethodName.put(ACTION_REGISTRATION, "registration");
		mapActionAndMethodName.put(ACTION_SEARCH, "search");
		return mapActionAndMethodName;
	}

	public static HashMap<String, String> mapActionUserPage() {
		HashMap<String, String> mapActionAndMethodName = new HashMap<>();
		mapActionAndMethodName.put(ACTION_LOGOUT, "logout");
		mapActionAndMethodName.put(ACTION_GET_USER_PROFILE, "getUserProfile");
		mapActionAndMethodName.put(ACTION_UPDATE_USER_PROFILE, "updateUserProfile");
		return mapActionAndMethodName;
	}

	public static HashMap<String, String> mapActionMoviePage() {
		HashMap<String, String> mapActionAndMethodName = new HashMap<>();
		mapActionAndMethodName.put(ACTION_ADD_TO_FAVORITE, "addMovieToFavorite");

		return mapActionAndMethodName;
	}

	public static String getLink(WebDriver driver, String url) throws IOException {
		try {
			driver.get(url);

			By sourceVideoLocator = By.xpath(".//div[@id='divVideoPlayer']//source");

			waitUntilVisible(driver, sourceVideoLocator);

			WebElement source = driver.findElement(sourceVideoLocator);
			String newMovieLink = source.getAttribute("src");
			if (newMovieLink != null) {
				return newMovieLink;
			}
			return null;
		} catch (Exception e) {
			System.out.println("Error when getlink: " + url);
			e.printStackTrace();
			if (driver != null) {
				driver.quit();
			}
			return null;
		}

	}

	public static boolean isValidUrl(String imagePath) throws MalformedURLException, IOException {
		try {
			URL url = new URL(imagePath);
			URLConnection urlConnection = url.openConnection();
			HttpURLConnection.setFollowRedirects(false);
			HttpURLConnection httpURLConnection = (HttpURLConnection) urlConnection;
			httpURLConnection.setRequestMethod("GET");
			httpURLConnection.setRequestProperty("User-Agent", "Mozilla/5.0");
			if (httpURLConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
				return true;
			}
			return false;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}

	}

	public static String getMAC() throws SocketException {
		StringBuilder sb = new StringBuilder();
		try {
			Enumeration<NetworkInterface> networks = NetworkInterface
					.getNetworkInterfaces();
			while (networks.hasMoreElements()) {
				NetworkInterface network = networks.nextElement();
				byte[] mac = network.getHardwareAddress();
				if (mac != null) {
					System.out.print("Current MAC address : ");
					for (int i = 0; i < mac.length; i++) {
						sb.append(String.format("%02X%s", mac[i],
								(i < mac.length - 1) ? "-" : ""));
					}
				}
			}
		} catch (SocketException e){
	    e.printStackTrace();
	  }
		return sb.toString();
	}
	
	public static String getPhanTomJSPath() throws SocketException{
		String macLapTop = "DC-85-DE-9C-1A-96";
		if(getMAC().contains(macLapTop)){
			return Constants.PATH_PHANTOM_JS_LAPTOP;
		}
		return Constants.PATH_PHANTOM_JS_PC;
	}
	static void waitUntilVisible(WebDriver driver, By selectClassLocator) {/*
		  WebDriverWait driverWait = new WebDriverWait(driver, 30);
		  driverWait.until(ExpectedConditions
		    .presenceOfElementLocated(selectClassLocator));
		 */}
	/*public static void waitUntilVisible(WebDriver driver, By selectClassLocator) {
		try {
		WebDriverWait driverWait = new WebDriverWait(driver, 60);
		driverWait.until(ExpectedConditions.or(
			    ExpectedConditions.visibilityOfElementLocated(selectClassLocator)));
		} catch (Exception e) {
			// No need action
		}
	}*/

	public static void killPhantomJS() throws IOException{
		for (int j = 0; j < 30; j++) {
			Runtime.getRuntime().exec("taskkill /F /IM phantomjs.exe");
		}
	}
	public static void sleep(int second){
		try {
			Thread.sleep(second*1000);
		} catch (InterruptedException e) {
		}
	}
	public static String removeAccent(String str) {
		  String temp = Normalizer.normalize(str, Normalizer.Form.NFD);
		  Pattern pattern = Pattern.compile("\\p{InCombiningDiacriticalMarks}+");
		  return pattern.matcher(temp).replaceAll("").replaceAll("Đ", "D").replaceAll("đ", "d")
				  .replaceAll("[^a-zA-Z0-9.-]", "-").replaceAll("--", "-");
		 }
	public static void main(String[] args) throws SocketException {
		long milisecond = new Date(117, 1, 28, 23, 59, 59).getTime();
		Date date = new Date(milisecond);
		System.out.println(getMAC());
	}
}
