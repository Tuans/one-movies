package one.movies.utils;

/**
 * @author TuanNA34
 *
 */
public class Constants {
	public static final String MONGODB_URI = "mongodb.uri";
	public static final String MONGODB_DATABASE = "mongodb.database";
	public static final String DATE_FORMAT = "dateFormat";
	public static final String TIMEZONE = "timezone";
	public static final int SECOND_WAIT = 60;

	public static final String ACTION_LOGIN = "login";
	public static final String ACTION_REGISTRATION = "registration";
	public static final String ACTION_SEARCH = "search";

	public static final String ACTION_GET_USER_PROFILE = "getUserProfile";
	public static final String ACTION_UPDATE_USER_PROFILE = "updateUserProfile";
	public static final String ACTION_LOGOUT = "logout";

	public static final String ACTION_ADD_TO_FAVORITE = "addMovieToFavorite";

	public static final String USER_PROFILE_PAGE = "userProfilePage";
	public static final String HOME_PAGE = "HomePage";

	public static final int RANGE_OF_RE_NEW_LINK = 15;

	public static final String PATH_PHANTOM_JS_LAPTOP = "D:\\Tools\\phantomjs-2.1.1-windows\\bin\\phantomjs.exe";
	public static final String PATH_PHANTOM_JS_PC = "D:\\Study\\Selenium\\phantomjs-2.1.1-windows\\bin\\phantomjs.exe";

	public static final String XPATH_MOVIE_TITLE = ".//span[@class='title-1']";
	public static final String XPATH_MOVIE_COUNTRY = ".//a[@class='country']";
	public static final String XPATH_MOVIE_YEAR = ".//dd[@class='movie-dd']/a";
	public static final String XPATH_MOVIE_GENRES = ".//dd[@class='movie-dd dd-cat']";
	public static final String XPATH_WATCH_MOVIE_BUTTON = ".//a[@id='btn-film-watch']";
	public static final String XPATH_MOVIE_IMAGE = ".//div[@class='movie-l-img']/img";
	public static final String XPATH_MOVIE_DESCRIPTION = ".//div[@id='film-content']/p";
	public static final String XPATH_MOVIE_CONTAINER = ".//li[@class='movie-item']/a";
}