package one.movies.utils;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

/**
 * 
 * @author TuanNA34
 *
 */
public class ConfigurationUtils {

	private static final String APP_PROP_FILE = "/app.properties";
	private static final String LOG4J_PROPERTIES = "/log4j.properties";
	private final static Logger log = Logger.getLogger(ConfigurationUtils.class);
	private static Properties props;

	public static Properties getProperties() {
		if (props == null) {
			props = new Properties();
			loadProperties();
		}
		return props;
	}

	private static void loadProperties() {
		try (InputStream propertiesStream = ConfigurationUtils.class.getResourceAsStream(APP_PROP_FILE);){
			props.load(propertiesStream);
			
			log.info("Properties Loaded successfully from " + APP_PROP_FILE);
		} catch (IOException e) {
			log.error("Properties did not load successfully", e);
		}
	}


	public static void configureLog4j() {
		Properties log4jProperties = new Properties();
		InputStream log4jStream = null;
		try {
			log4jStream = ConfigurationUtils.class.getResourceAsStream(LOG4J_PROPERTIES);
			log4jProperties.load(log4jStream);
			PropertyConfigurator.configure(log4jProperties);
		} catch (IOException e) {
			log.error("Cannot configure log4j properly: " + e.getMessage(), e);
		} finally {
			try {
				if (log4jStream != null) {
					log4jStream.close();
				}
			} catch (IOException e) {
				log.error(e.getMessage(), e);
			}
		}
	}

	
	public static String getProperty(String key) {
		return getProperties().getProperty(key);
	}

	
	public static String getProperty(String key, String defaultValue) {
		return getProperties().getProperty(key, defaultValue);
	}

	public static int getPropertyInt(String key, int defaultValue) {
		String value = getProperty(key, String.valueOf(defaultValue));
		try {
			return Integer.parseInt(value);
		} catch (Exception e) {
			return defaultValue;
		}
	}

}
