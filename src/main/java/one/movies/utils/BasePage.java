package one.movies.utils;

import java.util.ArrayList;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class BasePage {
	static WebDriver driver;
	static WebDriverWait driverWait;

	public void initial() {
		if(driver==null){
			driver = new ChromeDriver();
			
		}
		if(driverWait==null){
			driverWait = new WebDriverWait(driver, Constants.SECOND_WAIT);
		}
	}

	public void destroy() {
		if (driver != null) {
			driver.quit();
		}
	}

	public BasePage(WebDriver driver, WebDriverWait driverWait) {
		this.driver = driver;
		this.driverWait = driverWait;
	}

	public WebElement waitUtilVisible(By elementLocator) {
		WebElement element = driverWait.until(ExpectedConditions
				.presenceOfElementLocated(elementLocator));
		return element;
	}

	public void mySendKey(By elementLocator, String message) {
		WebElement element = waitUtilVisible(elementLocator);
		element.sendKeys(message);
	}

	public void myClick(By elementLocator) {
		WebElement element = waitUtilVisible(elementLocator);

		element.click();
	}

	public String myGetText(By elementLocator) {
		WebElement element = waitUtilVisible(elementLocator);
		return element.getText();
	}
	
	public void switchTab() {
		ArrayList<String> tabs2 = new ArrayList<String>(
				driver.getWindowHandles());
		if (tabs2.size() < 2)
			return;

		driver.switchTo().window(tabs2.get(0));
		driver.close();
		driver.switchTo().window(tabs2.get(1));
		FunctionUtils.sleep(2);
	}
}