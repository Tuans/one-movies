package one.movies.utils;

import static one.movies.utils.FunctionUtils.*;
import static one.movies.utils.MovieUtils.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FilenameUtils;
import org.bson.Document;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriverService;
import org.openqa.selenium.remote.DesiredCapabilities;

import one.movies.database.MovieDatabase;
import one.movies.model.Movie;


public class SyncMovieWatch8x {/*
	private static List<String> listMovieName = new ArrayList<String>();

	private static Movie getMovieLink(Movie movie, WebDriver driver,
			By sourceVideoLocator) {
		try {
			FunctionUtils.waitUntilVisible(driver, sourceVideoLocator);

			WebElement source = driver.findElement(sourceVideoLocator);
			String movieLink = source.getAttribute("src");
			movie.setMovieLink(movieLink);
			return movie;
		} catch (Exception e) {
			System.out.println("getMovieLink: Error");
			e.printStackTrace();
			return movie;
		}
	}

	public static Movie getStars(Movie movie, WebDriver driver,
			By movieStarsLocator) {
		try {
			String movieStars = "";
			FunctionUtils.waitUntilVisible(driver, movieStarsLocator);
			List<WebElement> allStars = driver.findElements(movieStarsLocator);
			for (WebElement stars : allStars) {
				movieStars = movieStars + stars.getText() + ", ";
			}
			movie.setStars(movieStars);
			return movie;
		} catch (Exception e) {
			e.printStackTrace();
			return movie;
		}
	}

	public static Movie getTags(Movie movie, WebDriver driver,
			By movieGenresLocator) {
		// In our site tags is genres of watch8x
		try {
			String movieGenres = "";
			FunctionUtils.waitUntilVisible(driver, movieGenresLocator);
			List<WebElement> allGenres = driver
					.findElements(movieGenresLocator);
			for (WebElement genres : allGenres) {
				movieGenres = movieGenres + genres.getText() + ", ";
			}
			movie.setTags(movieGenres);
			return movie;
		} catch (Exception e) {
			e.printStackTrace();
			return movie;
		}
	}

	public static Movie getImageLink(Movie movie, WebDriver driver,
			By largeImageLocator) {
		try {
			FunctionUtils.waitUntilVisible(driver, largeImageLocator);
			String largeImageLink = driver.findElement(largeImageLocator)
					.getAttribute("src");
			movie.setLargeImageLink(largeImageLink);
			System.out.println(largeImageLink);
			if (isValidUrl(largeImageLink)) {
				String fileExtension = FilenameUtils
						.getExtension(largeImageLink);
				largeImageLink = largeImageLink.replace("mvImg", "mvImgPS");
				String smallImageLink = largeImageLink.substring(0,
						largeImageLink.lastIndexOf("."));
				String lastTwoCharacter = smallImageLink.length() > 2 ? smallImageLink
						.substring(smallImageLink.length() - 2)
						: smallImageLink;
				if ("pl".equals(lastTwoCharacter)) {
					smallImageLink = smallImageLink.replace("pl", "ps");
				} else {
					smallImageLink = smallImageLink + "ps";
				}
				smallImageLink = smallImageLink + "." + fileExtension;
				System.out.println(smallImageLink);

				movie.setSmallImageLink(smallImageLink);
			}
			return movie;
		} catch (Exception e) {
			e.printStackTrace();
			return movie;
		}
	}

	public static Movie getMoviePath(Movie movie, WebDriver driver,
			By moviePathLocator) {
		try {
			FunctionUtils.waitUntilVisible(driver, moviePathLocator);
			String moviePath = driver.findElement(moviePathLocator).getText()
					.trim();
			String movieName = moviePath.substring(0, moviePath.indexOf(" "));
			String movieDescription = moviePath.substring(moviePath
					.indexOf(" ") + 1);
			movie.setMovieName(movieName);
			movie.setMovieDescription(movieDescription);
			return movie;
		} catch (Exception e) {
			e.printStackTrace();
			return movie;
		}
	}

	public static Movie getMovie(WebDriver driver, String url) {
		try {
			if(!isValidUrl(url)) return null;
			
			MovieDatabase movieDatabase = new MovieDatabase();
			listMovieName = movieDatabase.getListMovieName(new Document(
					"Genres", "21+"));

			DesiredCapabilities caps = new DesiredCapabilities();
			caps.setJavascriptEnabled(true);
			caps.setCapability(
					PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY,getPhanTomJSPath());
			driver = new PhantomJSDriver(caps);
			driver.get(url);
			Movie movie = new Movie();
			movie.setGenres("21+");
			movie.setReleaseYear("2017");
			movie.setOldSite(url);
			// get movie name and movie description
			By moviePathLocator = By.xpath(".//span[@id='divMoviePath1']");
			movie = getMoviePath(movie, driver, moviePathLocator);
			// if
			if (listMovieName.contains(movie.getMovieName())) {
				System.out.println("Existed movie with name: " + movie.getMovieName());
				if (driver != null) {
					driver.close();
				}
				return null;
			} else {
				listMovieName.add(movie.getMovieName());
			}
			// get large and small image link
			By largeImageLocator = By.xpath(".//img[@id='divMovieImage']");
			movie = getImageLink(movie, driver, largeImageLocator);
			// get genres
			By movieGenresLocator = By.xpath(".//td[@id='divMovieGenres']");
			movie = getTags(movie, driver, movieGenresLocator);
			// get stars
			By movieStarsLocator = By.xpath(".//span[@id='divMovieActors']");
			movie = getStars(movie, driver, movieStarsLocator);
			// get movie link
			By sourceVideoLocator = By
					.xpath(".//div[@id='divVideoPlayer']//source");
			movie = getMovieLink(movie, driver, sourceVideoLocator);

			movie.setOldSite(url);

			if (driver != null) {
				driver.close();
			}
			return movie;
		} catch (Exception e) {
			e.printStackTrace();
			if (driver != null) {
				driver.quit();
			}
			return null;
		}

	}

	public static void getMovies(WebDriver driver, String url, int page) throws InterruptedException {
		MovieDatabase movieDatabase = new MovieDatabase();

		DesiredCapabilities caps = new DesiredCapabilities();
		caps.setJavascriptEnabled(true);
		caps.setCapability(
				PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY, getPhanTomJSPath());
		driver = new PhantomJSDriver(caps);
		driver.get(url);
		if(page>0){
			By pageTargetLocator = By.xpath(".//div[@class='pagination']/a[contains(.,'" + page +"')]");
			FunctionUtils.waitUntilVisible(driver, pageTargetLocator);
			WebElement btnTargetPage = driver.findElement(pageTargetLocator);
			btnTargetPage.click();
			Thread.sleep(10*1000);
		}
		By movieContainer = By.xpath(".//div[@id='divMovieContainer']//a");
		FunctionUtils.waitUntilVisible(driver, movieContainer);

		List<WebElement> movieItems = driver.findElements(movieContainer);
		for (WebElement movieItem : movieItems) {
			String urlMovie = movieItem.getAttribute("href");
			Movie movie = getMovie(driver, urlMovie);
			if (isValidMovie(movie)) {
				boolean isSuccess = movieDatabase.addMovie(movie);
				if (isSuccess) {
					System.out.println("Inserted a movie: " + movie.toString());
					listMovieName.add(movie.getMovieName());
				}
			}
		}
	}

	public static void main(String[] args) throws IOException, InterruptedException {
		long startTime = System.currentTimeMillis();
		String url = "http://watch8x.com/Search_Underwear_Gen.aspx";
		WebDriver driver = null;
		getMovies(driver, url, 4);
		long endTime = System.currentTimeMillis();
		long totalTime = endTime - startTime;
		System.out.println("totalTime: " + totalTime);
		for (int i = 0; i < 30; i++) {
			Runtime.getRuntime().exec("taskkill /F /IM phantomjs.exe");
		}
	}

*/}
