package one.movies.utils;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.MongoDatabase;
import org.apache.log4j.Logger;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;
import one.movies.utils.Constants;
/**
 * 
 * @author TuanNA34
 *
 */
public class DatabaseUtils {

	private final static Logger log = Logger.getLogger(DatabaseUtils.class);

	public static MongoClient getDatabaseClient() {
		log.debug("Initialize a database client");
		MongoClientURI uri = new MongoClientURI(ConfigurationUtils.getProperties().getProperty(Constants.MONGODB_URI));
		return new MongoClient(uri);
	}

	public static MongoDatabase loadDatabase(final String databaseName) {
		MongoClient databaseClient = getDatabaseClient();
		return databaseClient.getDatabase(databaseName);
	}

	public static String getDateString(final Date date) {
		SimpleDateFormat sdf = new SimpleDateFormat(ConfigurationUtils.getProperties().getProperty(Constants.DATE_FORMAT));
		sdf.setTimeZone(TimeZone.getTimeZone(ConfigurationUtils.getProperties().getProperty(Constants.TIMEZONE)));
		return sdf.format(date);
	}

	public static Date modifyDate(final Date date, final int amount, final int field) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(field, amount);

		return calendar.getTime();
	}

}
