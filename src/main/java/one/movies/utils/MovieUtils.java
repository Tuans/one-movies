package one.movies.utils;

import static one.movies.utils.Constants.RANGE_OF_RE_NEW_LINK;
import static one.movies.utils.FunctionUtils.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.bson.Document;

import one.movies.database.MovieDatabase;
import one.movies.model.Movie;

public class MovieUtils {
	public static Movie getMovieByName(String name) {
		MovieDatabase movieDatabase = new MovieDatabase();
		Movie movie = Movie.toMovie(movieDatabase.getMovieByCorrectedName(name));
		return movie;
	}
	
	public static boolean isValidMovie(Movie movie) {
		if (movie != null && movie.getMovieName() != null && movie.getMovieDescription() != null
				&& movie.getMovieLink() != null && movie.getSmallImageLink() != null
				&& movie.getLargeImageLink() != null)
			return true;

		return false;
	}
	
	public static void RenewMovieLink() throws IOException, InterruptedException {
		killPhantomJS();
		MovieDatabase movieDatabase = new MovieDatabase();
		int totalMovies = movieDatabase.getListMovie(new Document("Genres", "21+")).size();
		System.out.println("totalMovies: " + totalMovies);
		for (int i = 0; i < totalMovies; i += RANGE_OF_RE_NEW_LINK) {
			new MovieDatabase().reNewLink(i, RANGE_OF_RE_NEW_LINK);
			killPhantomJS();
			int numberOfMovieGotLink = (i + RANGE_OF_RE_NEW_LINK) > totalMovies ? totalMovies
					: (i + RANGE_OF_RE_NEW_LINK);
			System.out.println("Got new link for " + numberOfMovieGotLink + " movies");
			Thread.sleep(5 * 1000);
		}
	}
	
	public static List<Movie> getListMovieByYear(String year, int limit){
		List<Movie> lst = new ArrayList<Movie>();
		if(year!=null && !year.isEmpty()){
			Document filter = new Document("ReleaseYear", year);
			if(limit>-1){
				filter.append("Limit", limit);
			}
			MovieDatabase movieDatabase = new MovieDatabase();
			lst = movieDatabase.getListMovie(filter);
		}
		return lst;
	}
	
	public static List<Movie> getListMovieByCountry(String country, int limit){
		List<Movie> lst = new ArrayList<Movie>();
		if(country!=null && !country.isEmpty()){
			Document filter = new Document("Country", country);
			if(limit>-1){
				filter.append("Limit", limit);
			}
			MovieDatabase movieDatabase = new MovieDatabase();
			lst = movieDatabase.getListMovie(filter);
		}
		return lst;
	}
	
	public static List<Movie> getListMovieByStars(String star, int limit){
		List<Movie> lst = new ArrayList<Movie>();
		if(star!=null && !star.isEmpty()){
			Document filter = new Document("Stars", new Document("$regex", star));
			if(limit>-1){
				filter.append("Limit", limit);
			}
			MovieDatabase movieDatabase = new MovieDatabase();
			lst = movieDatabase.getListMovie(filter);
		}
		return lst;
	}
	
	public static List<Movie> getListMovieByGenres(String genre, int limit){
		List<Movie> lst = new ArrayList<Movie>();
		if(genre!=null && !genre.isEmpty()){
			Document filter = new Document("Genres", new Document("$regex", genre));
			if(limit>-1){
				filter.append("Limit", limit);
			}
			MovieDatabase movieDatabase = new MovieDatabase();
			lst = movieDatabase.getListMovie(filter);
		}
		return lst;
	}
	
	public static List<Movie> getListMovieByTags(String tag, int limit){
		List<Movie> lst = new ArrayList<Movie>();
		if(tag!=null && !tag.isEmpty()){
			Document filter = new Document("Tags", new Document("$regex", tag));
			if(limit>-1){
				filter.append("Limit", limit);
			}
			MovieDatabase movieDatabase = new MovieDatabase();
			lst = movieDatabase.getListMovie(filter);
		}
		return lst;
	}
	
	public static List<Movie> getListMovieUpNext(Movie movie){
		List<Movie> lst = new ArrayList<Movie>();
		List<String> lstTags = movie.ToList("Tags");
		String tag = lstTags.size()>0?lstTags.get(0):"";
		List<String> lstGenres = movie.ToList("Genres");
		String genre = lstGenres.size()>0?lstGenres.get(0):"";
		
		if(tag!=null && !tag.isEmpty()){
			return getListMovieByTags(tag, 10);
		}else if(genre!=null && !genre.isEmpty()){
			return getListMovieByGenres(genre, 10);
		}
		return lst;
	}
	
	public static void main(String[] args) throws IOException, InterruptedException {
		RenewMovieLink();
	}
	
}
