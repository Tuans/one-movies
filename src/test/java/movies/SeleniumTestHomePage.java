package movies;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class SeleniumTestHomePage {
	public static void testLogin(){
		//Khoi tao va bat web browser: Dung them lenh chi ro duong dan chua Firefox neu dung version >=3
				//System.setProperty("webdriver.gecko.driver", "C:\Users\FWA\Downloads\geckodriver-v0.14.0-win64")
				WebDriver driver = new FirefoxDriver();
				
				//Di den URL
				driver.get("http://localhost:8080/movies/movie");
				//Click button login
				WebElement btnPopupLogin = driver.findElement(By.xpath("html/body/div[1]/div/div[3]/ul/li[2]/a"));
				btnPopupLogin.click();
				
				//Input username
				WebElement txtUsername= driver.findElement(By.xpath(".//*[@id='myModal']/div/div/section/div/div/div/div[2]/form/input[1]"));
				txtUsername.sendKeys("user001");
				
				//Input password
				WebElement txtPassword = driver.findElement(By.xpath(".//*[@id='myModal']/div/div/section/div/div/div/div[2]/form/input[2]"));
				txtPassword.sendKeys("pass001");
							
				//Click button login
				WebElement btnLogin = driver.findElement(By.xpath(".//*[@id='myModal']/div/div/section/div/div/div/div[2]/form/input[3]"));
				btnLogin.click();
	}
	public static void main(String[] args) {
		testLogin();
	}
}
