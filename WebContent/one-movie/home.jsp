<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="java.util.List, org.bson.Document, java.util.ArrayList, one.movies.model.Movie" %>

<!DOCTYPE html>
<html lang="en">
<head>
<title>One Movie</title>
<!-- for-mobile-apps -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords"
	content="One Movies Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<script type="application/x-javascript">
	 addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
		function hideURLbar(){ window.scrollTo(0,1); } 
</script>
<!-- //for-mobile-apps -->
<link href="${pageContext.request.contextPath}/admin/css/bootstrap.css" rel="stylesheet" type="text/css"
	media="all" />
<link href="${pageContext.request.contextPath}/admin/css/style.css" rel="stylesheet" type="text/css" media="all" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/admin/css/contactstyle.css" type="text/css"
	media="all" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/admin/css/faqstyle.css" type="text/css"
	media="all" />
<link href="${pageContext.request.contextPath}/admin/css/single.css" rel='stylesheet' type='text/css' />
<link href="${pageContext.request.contextPath}/admin/css/medile.css" rel='stylesheet' type='text/css' />
<!-- banner-slider -->
<link href="${pageContext.request.contextPath}/admin/css/jquery.slidey.min.css" rel="stylesheet">
<!-- //banner-slider -->
<!-- pop-up -->
<link href="${pageContext.request.contextPath}/admin/css/popuo-box.css" rel="stylesheet" type="text/css"
	media="all" />
<!-- //pop-up -->
<!-- font-awesome icons -->
<link rel="stylesheet" href="${pageContext.request.contextPath}/admin/css/font-awesome.min.css" />
<!-- //font-awesome icons -->
<!-- js -->
<script type="text/javascript" src="${pageContext.request.contextPath}/admin/js/jquery-2.1.4.min.js"></script>
<!-- //js -->
<!-- banner-bottom-plugin -->
<link href="${pageContext.request.contextPath}/admin/css/owl.carousel.css" rel="stylesheet" type="text/css"
	media="all">
<script src="${pageContext.request.contextPath}/admin/js/owl.carousel.js"></script>
<script>
	$(document).ready(function() {
		$("#owl-demo").owlCarousel({
			autoPlay : 5000, //Set AutoPlay to 5 seconds
			items : 5,
			itemsDesktop : [ 640, 4 ],
			itemsDesktopSmall : [ 414, 3 ]
		});

	});
</script>
<!-- //banner-bottom-plugin -->
<link
	href='//fonts.googleapis.com/css?family=Roboto+Condensed:400,700italic,700,400italic,300italic,300'
	rel='stylesheet' type='text/css'>
<!-- start-smoth-scrolling -->
<script type="text/javascript" src="${pageContext.request.contextPath}/admin/js/move-top.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/admin/js/easing.js"></script>
<script type="text/javascript">
	jQuery(document).ready(function($) {
		$(".scroll").click(function(event) {
			event.preventDefault();
			$('html,body').animate({
				scrollTop : $(this.hash).offset().top
			}, 1000);
		});
	});
</script>
<!-- start-smoth-scrolling -->
</head>
<%
		List<Movie> lstMovie = (ArrayList<Movie>) request.getAttribute("lstMovie");
		boolean isClickLoginPopup = (((String) request.getAttribute("loginErrorMessage"))!=null
				 || ((String) request.getAttribute("registrationErrorMessage"))!=null)?true:false;
		boolean isClickRegistrationTab = ((String) request.getAttribute("registrationErrorMessage"))!=null?true:false;
%>
<body>
<%@include  file="nav.jsp" %>
	<!-- banner -->

	<script src="${pageContext.request.contextPath}/admin/js/jquery.slidey.js"></script>
	<script src="${pageContext.request.contextPath}/admin/js/jquery.dotdotdot.min.js"></script>
	<script type="text/javascript">
		$("#slidey").slidey({
			interval : 8000,
			listCount : 5,
			autoplay : false,
			showList : true
		});
		$(".slidey-list-description").dotdotdot();
	</script>
	<!-- //banner -->

	<div class="general_social_icons">
		<nav class="social">
			<ul>
				<li class="w3_twitter"><a href="#">Twitter <i
						class="fa fa-twitter"></i></a></li>
				<li class="w3_facebook"><a href="#">Facebook <i
						class="fa fa-facebook"></i></a></li>
				<li class="w3_dribbble"><a href="#">Dribbble <i
						class="fa fa-dribbble"></i></a></li>
				<li class="w3_g_plus"><a href="#">Google+ <i
						class="fa fa-google-plus"></i></a></li>
			</ul>
		</nav>
	</div>
	<!-- banner -->
	
	<div id="slidey" style="display: none;">
		<ul>
		<% if(lstMovie!=null) for(Movie movie: lstMovie){		
		%>
			<li><img src="<%=movie.getLargeImageLink() %>" alt=" ">
				<p class='title'><%=movie.getMovieName() %></p>
				<p class='description'><%=movie.getMovieDescription() %></p></li>
		<%
		}
		%>
			
		</ul>
	</div>
	<%if(isClickLoginPopup){ %>
		<script type="text/javascript">
			$(document).ready(function(){
			  $('#btnPopupLogin').trigger('click');
			});
		</script>
	<%} %>
	<script src="${pageContext.request.contextPath}/admin/js/jquery.slidey.js"></script>
	<script src="${pageContext.request.contextPath}/admin/js/jquery.dotdotdot.min.js"></script>
	<script type="text/javascript">
		$("#slidey").slidey({
			interval : 8000,
			listCount : 5,
			autoplay : false,
			showList : true
		});
		$(".slidey-list-description").dotdotdot();
	</script>
	<!-- //banner -->
	<!-- banner-bottom -->
	<div class="banner-bottom">
		<div class="container">
			<div class="w3_agile_banner_bottom_grid">
				<div id="owl-demo" class="owl-carousel owl-theme">
				<% if(lstMovie!=null) for(Movie movie: lstMovie){		
				%>
					<div class="item">
						<div class="w3l-movie-gride-agile w3l-movie-gride-agile1">
							<a href="${pageContext.request.contextPath}/watch?movieName=<%=movie.getCorrectedName() %>" class="hvr-shutter-out-horizontal"><img
								src="<%=movie.getSmallImageLink() %>" title="<%=movie.getMovieDescription() %>" class="img-responsive"
								alt=" "/>
								<div class="w3l-action-icon">
									<i class="fa fa-play-circle" aria-hidden="true"></i>
								</div> </a>
							<div class="mid-1 agileits_w3layouts_mid_1_home">
								<div class="w3l-movie-text">
									<h6>
										<a href="${pageContext.request.contextPath}/watch?movieName=<%=movie.getCorrectedName() %>"><%=movie.getMovieName() %></a>
									</h6>
								</div>
								<div class="mid-2 agile_mid_2_home">
									<p><%=movie.getReleaseYear() %></p>
									<div class="block-stars">
										<ul class="w3l-ratings">
											<li><a href="#"><i class="fa fa-star"
													aria-hidden="true"></i></a></li>
											<li><a href="#"><i class="fa fa-star"
													aria-hidden="true"></i></a></li>
											<li><a href="#"><i class="fa fa-star"
													aria-hidden="true"></i></a></li>
											<li><a href="#"><i class="fa fa-star"
													aria-hidden="true"></i></a></li>
											<li><a href="#"><i class="fa fa-star-half-o"
													aria-hidden="true"></i></a></li>
										</ul>
									</div>
									<div class="clearfix"></div>
								</div>
							</div>
							<div class="ribben">
								<p>NEW</p>
							</div>
						</div>
					</div>
					<%} %>
				</div>
			</div>
		</div>
	</div>
	<!-- //banner-bottom -->


	<!-- general -->
	<div class="general">
		<h4 class="latest-text w3_latest_text">Featured Movies</h4>
		<div class="container">
			<div class="bs-example bs-example-tabs" role="tabpanel"
				data-example-id="togglable-tabs">
				<ul id="myTab" class="nav nav-tabs" role="tablist">
					<li role="presentation" class="active"><a href="#home"
						id="home-tab" role="tab" data-toggle="tab" aria-controls="home"
						aria-expanded="true">Featured</a></li>
					<li role="presentation"><a href="#profile" role="tab"
						id="profile-tab" data-toggle="tab" aria-controls="profile"
						aria-expanded="false">Top viewed</a></li>
					<li role="presentation"><a href="#rating" id="rating-tab"
						role="tab" data-toggle="tab" aria-controls="rating"
						aria-expanded="true">Top Rating</a></li>
					<li role="presentation"><a href="#imdb" role="tab"
						id="imdb-tab" data-toggle="tab" aria-controls="imdb"
						aria-expanded="false">Recently Added</a></li>
				</ul>
				<div id="myTabContent" class="tab-content">
					<div role="tabpanel" class="tab-pane fade active in" id="home"
						aria-labelledby="home-tab">
						<div class="w3_agile_featured_movies">
						<% if(lstMovie!=null) for(Movie movie: lstMovie){		
						%>
							<div class="col-md-2 w3l-movie-gride-agile">
								<a href="${pageContext.request.contextPath}/watch?movieName=<%=movie.getCorrectedName() %>" class="hvr-shutter-out-horizontal"><img
									src="<%=movie.getSmallImageLink() %>" title="<%=movie.getMovieDescription() %>" class="img-responsive img-movie"
									alt=" "/>
									<div class="w3l-action-icon">
										<i class="fa fa-play-circle" aria-hidden="true"></i>
									</div> </a>
								<div class="mid-1 agileits_w3layouts_mid_1_home">
									<div class="w3l-movie-text">
										<h6>
											<a href="${pageContext.request.contextPath}/watch?movieName=<%=movie.getCorrectedName() %>" 
											<% if(movie.getMovieName().length()>20) {%>style="font-size: 0.9em" <%} else { %>style="font-size: 1.3em" <%} %>>
												<%=movie.getMovieName() %>
											</a>
										</h6>
									</div>
									
								</div>
								<div class="ribben">
								<p><%=movie.getReleaseYear() %></p>
							</div>
							</div>
						<%} %>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- //general -->

	<%@include  file="footer.jsp" %>
</body>
</html>