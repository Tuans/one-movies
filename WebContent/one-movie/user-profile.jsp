<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page
	import="java.util.List, org.bson.Document, java.util.ArrayList, one.movies.model.User"%>

<!DOCTYPE html>
<html lang="en">
<head>
<title>User Profile</title>
<!-- for-mobile-apps -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords"
	content="One Movies Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />

<!-- //for-mobile-apps -->
<link href="${pageContext.request.contextPath}/admin/css/bootstrap.css"
	rel="stylesheet" type="text/css" media="all" />
<link href="${pageContext.request.contextPath}/admin/css/style.css"
	rel="stylesheet" type="text/css" media="all" />
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/admin/css/contactstyle.css"
	type="text/css" media="all" />
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/admin/css/faqstyle.css"
	type="text/css" media="all" />
<link href="${pageContext.request.contextPath}/admin/css/single.css"
	rel='stylesheet' type='text/css' />
<link href="${pageContext.request.contextPath}/admin/css/medile.css"
	rel='stylesheet' type='text/css' />
<!-- banner-slider -->
<link
	href="${pageContext.request.contextPath}/admin/css/jquery.slidey.min.css"
	rel="stylesheet">
<!-- //banner-slider -->
<!-- pop-up -->
<link href="${pageContext.request.contextPath}/admin/css/popuo-box.css"
	rel="stylesheet" type="text/css" media="all" />
<!-- //pop-up -->
<!-- font-awesome icons -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/admin/css/font-awesome.min.css" />
<!-- //font-awesome icons -->
<!-- js -->
<script type="text/javascript"
	src="${pageContext.request.contextPath}/admin/js/jquery-2.1.4.min.js"></script>
<!-- //js -->
<link href='//fonts.googleapis.com/css?family=Questrial'
	rel='stylesheet' type='text/css'>
<link
	href='//fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic'
	rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="css/j-forms-profile.css">
<link href="css/style-profile.css" rel="stylesheet" type="text/css"
	media="all" />

<!-- banner-bottom-plugin -->
<link
	href="${pageContext.request.contextPath}/admin/css/owl.carousel.css"
	rel="stylesheet" type="text/css" media="all">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/admin/js/jquery-2.1.4.min.js"></script>

<script type="text/javascript"
	src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css" />
</head>
<%
	User userLogged = (User) session.getAttribute("user");
	String updateUserProfileErrorMessage = (String) request.getAttribute("updateUserProfileErrorMessage");
	String updateUserProfileSuccessMessage = (String) request.getAttribute("updateUserProfileSuccessMessage");
%>
<body>
	<!-- banner -->
	<%@include file="nav.jsp"%>
	<!-- banner -->
	<div class="container">
		<h2>User Profile</h2>
		<form class="well form-horizontal" action="#" method="post" id="updateUserProfile">
			<%if(updateUserProfileErrorMessage!=null){ %>
				<div class="alert alert-danger fade in">
									<a href="#" class="close" data-dismiss="alert">&times;</a> <strong>Error!</strong>
									<%=updateUserProfileErrorMessage%>
								</div>
			<%} %>
			
			<%if(updateUserProfileSuccessMessage!=null){ %>
				<div class="alert alert-success fade in">
									<a href="#" class="close" data-dismiss="alert">&times;</a> <strong>Success!</strong>
									<%=updateUserProfileSuccessMessage%>
								</div>
			<%} %>
			<!-- Text input-->
			<div class="form-group">
				<label class="col-md-4 control-label">User Name</label>
				<div class="col-md-4 inputGroupContainer">
					<div class="input-group">
						<span class="input-group-addon"><i
							class="glyphicon glyphicon-user"></i></span> <input name="userName" id="userName"
							class="form-control" type="text" readonly value="<%= userLogged.getUserName()%>">
					</div>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-4 control-label">Display Name</label>
				<div class="col-md-4 inputGroupContainer">
					<div class="input-group">
						<span class="input-group-addon"><i
							class="glyphicon glyphicon-user"></i></span> <input name="displayName"
							class="form-control" type="text" value="<%=userLogged.getDisplayName()!=null?userLogged.getDisplayName():""%>">
					</div>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-4 control-label">Password</label>
				<div class="col-md-4 inputGroupContainer">
					<div class="input-group">
						<span class="input-group-addon"><i
							class="glyphicon glyphicon-eye-close"></i></span> <input name="password"
							class="form-control" type="password">
					</div>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-4 control-label">Phone Number</label>
				<div class="col-md-4 inputGroupContainer">
					<div class="input-group">
						<span class="input-group-addon"><i
							class="glyphicon glyphicon-phone"></i></span> <input name="phoneNumber"
							class="form-control" type="text" readonly value="<%= userLogged.getPhoneNumber()%>">
					</div>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-4 control-label">Email</label>
				<div class="col-md-4 inputGroupContainer">
					<div class="input-group">
						<span class="input-group-addon"><i
							class="glyphicon glyphicon-envelope"></i></span> <input name="email"
							class="form-control" type="text" value="<%=userLogged.getEmail()!=null?userLogged.getEmail():""%>">
					</div>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-4 control-label">Date of Birth</label>
				<div class="col-md-4 inputGroupContainer">
					<div class="input-group">
						<span class="input-group-addon"><i
							class="glyphicon glyphicon-calendar"></i></span> <input id="dateOfBirth"
							name="dateOfBirth" class="form-control" type="text"
							placeholder="dd/MM/yyyy" value="<%= userLogged.getDateOfBirth()!=null?userLogged.getDateOfBirth():""%>" readonly>
					</div>
					<script>
						$(document)
								.ready(
										function() {
											var date_input = $('input[name="dateOfBirth"]'); //our date input has the name "date"
											var container = $('.bootstrap-iso form').length > 0 ? $(
													'.bootstrap-iso form')
													.parent()
													: "body";
											date_input.datepicker({
												format : 'dd/mm/yyyy',
												container : container,
												todayHighlight : true,
												autoclose : true,
											})
										})
					</script>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-4 control-label">Country</label>
				<div class="col-md-4 inputGroupContainer">
					<div class="input-group">
						<span class="input-group-addon"><i
							class="glyphicon glyphicon-flag"></i></span> <select
							class="form-control" name="country">
							<option value="" <%if(userLogged.getCountry()==null){%>selected="selected" <%} %>>Please select your country</option>
							<option value="AM" <%if("AM".equals(userLogged.getCountry())){%>selected="selected" <%} %>>Armenia</option>
							<option value="AW" <%if("AW".equals(userLogged.getCountry())){%>selected="selected" <%} %>>Aruba</option>
							<option value="AU" <%if("AU".equals(userLogged.getCountry())){%>selected="selected" <%} %>>Australia</option>
							<option value="AT" <%if("AT".equals(userLogged.getCountry())){%>selected="selected" <%} %>>Austria</option>
							<option value="AZ" <%if("AZ".equals(userLogged.getCountry())){%>selected="selected" <%} %>>Azerbaijan</option>
							<option value="BS" <%if("BS".equals(userLogged.getCountry())){%>selected="selected" <%} %>>Bahamas</option>
							<option value="BH" <%if("BH".equals(userLogged.getCountry())){%>selected="selected" <%} %>>Bahrain</option>
							<option value="BD" <%if("BD".equals(userLogged.getCountry())){%>selected="selected" <%} %>>Bangladesh</option>
							<option value="BB" <%if("BB".equals(userLogged.getCountry())){%>selected="selected" <%} %>>Barbados</option>
							<option value="BR" <%if("BR".equals(userLogged.getCountry())){%>selected="selected" <%} %>>Brazil</option>
							<option value="IO" <%if("IO".equals(userLogged.getCountry())){%>selected="selected" <%} %>>British Indian Ocean Territory</option>
							<option value="BN" <%if("BN".equals(userLogged.getCountry())){%>selected="selected" <%} %>>Brunei Darussalam</option>
							<option value="BG" <%if("BG".equals(userLogged.getCountry())){%>selected="selected" <%} %>>Bulgaria</option>
							<option value="BF" <%if("BF".equals(userLogged.getCountry())){%>selected="selected" <%} %>>Burkina Faso</option>
							<option value="BI" <%if("BI".equals(userLogged.getCountry())){%>selected="selected" <%} %>>Burundi</option>
							<option value="KH" <%if("KH".equals(userLogged.getCountry())){%>selected="selected" <%} %>>Cambodia</option>
							<option value="CM" <%if("CM".equals(userLogged.getCountry())){%>selected="selected" <%} %>>Cameroon</option>
							<option value="CA" <%if("CA".equals(userLogged.getCountry())){%>selected="selected" <%} %>>Canada</option>
							<option value="CV" <%if("CV".equals(userLogged.getCountry())){%>selected="selected" <%} %>>Cape Verde</option>
							<option value="KY" <%if("KY".equals(userLogged.getCountry())){%>selected="selected" <%} %>>Cayman Islands</option>
							<option value="CF" <%if("CF".equals(userLogged.getCountry())){%>selected="selected" <%} %>>Central African Republic</option>
							<option value="CO" <%if("CO".equals(userLogged.getCountry())){%>selected="selected" <%} %>>Colombia</option>
							<option value="KM" <%if("KM".equals(userLogged.getCountry())){%>selected="selected" <%} %>>Comoros</option>
							<option value="FJ" <%if("FJ".equals(userLogged.getCountry())){%>selected="selected" <%} %>>Fiji</option>
							<option value="FI" <%if("FI".equals(userLogged.getCountry())){%>selected="selected" <%} %>>Finland</option>
							<option value="FR" <%if("FR".equals(userLogged.getCountry())){%>selected="selected" <%} %>>France</option>
							<option value="FX" <%if("FX".equals(userLogged.getCountry())){%>selected="selected" <%} %>>France, Metropolitan</option>
							<option value="GF" <%if("GF".equals(userLogged.getCountry())){%>selected="selected" <%} %>>French Guiana</option>
							<option value="PF" <%if("PF".equals(userLogged.getCountry())){%>selected="selected" <%} %>>French Polynesia</option>
							<option value="TF" <%if("TF".equals(userLogged.getCountry())){%>selected="selected" <%} %>>French Southern Territories</option>
							<option value="GA" <%if("GA".equals(userLogged.getCountry())){%>selected="selected" <%} %>>Gabon</option>
							<option value="GM" <%if("GM".equals(userLogged.getCountry())){%>selected="selected" <%} %>>Gambia</option>
							<option value="GE" <%if("GE".equals(userLogged.getCountry())){%>selected="selected" <%} %>>Georgia</option>
							<option value="DE" <%if("DE".equals(userLogged.getCountry())){%>selected="selected" <%} %>>Germany</option>
							<option value="GN" <%if("GN".equals(userLogged.getCountry())){%>selected="selected" <%} %>>Guinea</option>
							<option value="GW" <%if("GW".equals(userLogged.getCountry())){%>selected="selected" <%} %>>Guinea-Bissau</option>
							<option value="GY" <%if("GY".equals(userLogged.getCountry())){%>selected="selected" <%} %>>Guyana</option>
							<option value="HT" <%if("HT".equals(userLogged.getCountry())){%>selected="selected" <%} %>>Haiti</option>
							<option value="HM" <%if("HM".equals(userLogged.getCountry())){%>selected="selected" <%} %>>Heard and Mc Donald Islands</option>
							<option value="VA" <%if("VA".equals(userLogged.getCountry())){%>selected="selected" <%} %>>Holy See (Vatican City State)</option>
							<option value="HN" <%if("HN".equals(userLogged.getCountry())){%>selected="selected" <%} %>>Honduras</option>
							<option value="HK" <%if("HK".equals(userLogged.getCountry())){%>selected="selected" <%} %>>Hong Kong</option>
							<option value="HU" <%if("HU".equals(userLogged.getCountry())){%>selected="selected" <%} %>>Hungary</option>
							<option value="IS" <%if("IS".equals(userLogged.getCountry())){%>selected="selected" <%} %>>Iceland</option>
							<option value="IN" <%if("IN".equals(userLogged.getCountry())){%>selected="selected" <%} %>>India</option>
							<option value="ID" <%if("ID".equals(userLogged.getCountry())){%>selected="selected" <%} %>>Indonesia</option>
							<option value="IR" <%if("IR".equals(userLogged.getCountry())){%>selected="selected" <%} %>>Iran (Islamic Republic of)</option>
							<option value="IQ" <%if("IQ".equals(userLogged.getCountry())){%>selected="selected" <%} %>>Iraq</option>
							<option value="IE" <%if("IE".equals(userLogged.getCountry())){%>selected="selected" <%} %>>Ireland</option>
							<option value="IL" <%if("IL".equals(userLogged.getCountry())){%>selected="selected" <%} %>>Israel</option>
							<option value="IT" <%if("IT".equals(userLogged.getCountry())){%>selected="selected" <%} %>>Italy</option>
							<option value="JM" <%if("JM".equals(userLogged.getCountry())){%>selected="selected" <%} %>>Jamaica</option>
							<option value="JP" <%if("JP".equals(userLogged.getCountry())){%>selected="selected" <%} %>>Japan</option>
							<option value="JO" <%if("JO".equals(userLogged.getCountry())){%>selected="selected" <%} %>>Jordan</option>
							<option value="KR" <%if("KR".equals(userLogged.getCountry())){%>selected="selected" <%} %>>Korea, Republic of</option>
							<option value="KW" <%if("KW".equals(userLogged.getCountry())){%>selected="selected" <%} %>>Kuwait</option>
							<option value="KG" <%if("KG".equals(userLogged.getCountry())){%>selected="selected" <%} %>>Kyrgyzstan</option>
							<option value="LA" <%if("LA".equals(userLogged.getCountry())){%>selected="selected" <%} %>>Lao People's Democratic Republic</option>
							<option value="LV" <%if("LV".equals(userLogged.getCountry())){%>selected="selected" <%} %>>Latvia</option>
							<option value="LT" <%if("LT".equals(userLogged.getCountry())){%>selected="selected" <%} %>>Lithuania</option>
							<option value="MK" <%if("MK".equals(userLogged.getCountry())){%>selected="selected" <%} %>>Macedonia, The Former Yugoslav
								Republic of</option>
							<option value="MY" <%if("MY".equals(userLogged.getCountry())){%>selected="selected" <%} %>>Malaysia</option>
							<option value="MV" <%if("MV".equals(userLogged.getCountry())){%>selected="selected" <%} %>>Maldives</option>
							<option value="ML" <%if("ML".equals(userLogged.getCountry())){%>selected="selected" <%} %>>Mali</option>
							<option value="YT" <%if("YT".equals(userLogged.getCountry())){%>selected="selected" <%} %>>Mayotte</option>
							<option value="MX" <%if("MX".equals(userLogged.getCountry())){%>selected="selected" <%} %>>Mexico</option>
							<option value="MS" <%if("MS".equals(userLogged.getCountry())){%>selected="selected" <%} %>>Montserrat</option>
							<option value="MA" <%if("MA".equals(userLogged.getCountry())){%>selected="selected" <%} %>>Morocco</option>
							<option value="MZ" <%if("MZ".equals(userLogged.getCountry())){%>selected="selected" <%} %>>Mozambique</option>
							<option value="MM" <%if("MM".equals(userLogged.getCountry())){%>selected="selected" <%} %>>Myanmar</option>
							<option value="NA" <%if("NA".equals(userLogged.getCountry())){%>selected="selected" <%} %>>Namibia</option>
							<option value="NR" <%if("NR".equals(userLogged.getCountry())){%>selected="selected" <%} %>>Nauru</option>
							<option value="NP" <%if("NP".equals(userLogged.getCountry())){%>selected="selected" <%} %>>Nepal</option>
							<option value="NL" <%if("NL".equals(userLogged.getCountry())){%>selected="selected" <%} %>>Netherlands</option>
							<option value="AN" <%if("AN".equals(userLogged.getCountry())){%>selected="selected" <%} %>>Netherlands Antilles</option>
							<option value="NC" <%if("NC".equals(userLogged.getCountry())){%>selected="selected" <%} %>>New Caledonia</option>
							<option value="NZ" <%if("NZ".equals(userLogged.getCountry())){%>selected="selected" <%} %>>New Zealand</option>
							<option value="NI" <%if("NI".equals(userLogged.getCountry())){%>selected="selected" <%} %>>Nicaragua</option>
							<option value="NE" <%if("NE".equals(userLogged.getCountry())){%>selected="selected" <%} %>>Niger</option>
							<option value="NG" <%if("NG".equals(userLogged.getCountry())){%>selected="selected" <%} %>>Nigeria</option>
							<option value="NU" <%if("NU".equals(userLogged.getCountry())){%>selected="selected" <%} %>>Niue</option>
							<option value="NF" <%if("NF".equals(userLogged.getCountry())){%>selected="selected" <%} %>>Norfolk Island</option>
							<option value="MP" <%if("MP".equals(userLogged.getCountry())){%>selected="selected" <%} %>>Northern Mariana Islands</option>
							<option value="NO" <%if("NO".equals(userLogged.getCountry())){%>selected="selected" <%} %>>Norway</option>
							<option value="OM" <%if("OM".equals(userLogged.getCountry())){%>selected="selected" <%} %>>Oman</option>
							<option value="PK" <%if("PK".equals(userLogged.getCountry())){%>selected="selected" <%} %>>Pakistan</option>
							<option value="PW" <%if("PW".equals(userLogged.getCountry())){%>selected="selected" <%} %>>Palau</option>
							<option value="PA" <%if("PA".equals(userLogged.getCountry())){%>selected="selected" <%} %>>Panama</option>
							<option value="PT" <%if("PT".equals(userLogged.getCountry())){%>selected="selected" <%} %>>Portugal</option>
							<option value="PR" <%if("PR".equals(userLogged.getCountry())){%>selected="selected" <%} %>>Puerto Rico</option>
							<option value="QA" <%if("QA".equals(userLogged.getCountry())){%>selected="selected" <%} %>>Qatar</option>
							<option value="RE" <%if("RE".equals(userLogged.getCountry())){%>selected="selected" <%} %>>Reunion</option>
							<option value="RO" <%if("RO".equals(userLogged.getCountry())){%>selected="selected" <%} %>>Romania</option>
							<option value="RU" <%if("RU".equals(userLogged.getCountry())){%>selected="selected" <%} %>>Russian Federation</option>
							<option value="WS" <%if("WS".equals(userLogged.getCountry())){%>selected="selected" <%} %>>Samoa</option>
							<option value="SM" <%if("SM".equals(userLogged.getCountry())){%>selected="selected" <%} %>>San Marino</option>
							<option value="ST" <%if("ST".equals(userLogged.getCountry())){%>selected="selected" <%} %>>Sao Tome and Principe</option>
							<option value="SA" <%if("SA".equals(userLogged.getCountry())){%>selected="selected" <%} %>>Saudi Arabia</option>
							<option value="SL" <%if("SL".equals(userLogged.getCountry())){%>selected="selected" <%} %>>Sierra Leone</option>
							<option value="SG" <%if("SG".equals(userLogged.getCountry())){%>selected="selected" <%} %>>Singapore</option>
							<option value="SK" <%if("SK".equals(userLogged.getCountry())){%>selected="selected" <%} %>>Slovakia (Slovak Republic)</option>
							<option value="SI" <%if("SI".equals(userLogged.getCountry())){%>selected="selected" <%} %>>Slovenia</option>
							<option value="SB" <%if("SB".equals(userLogged.getCountry())){%>selected="selected" <%} %>>Solomon Islands</option>
							<option value="SO" <%if("SO".equals(userLogged.getCountry())){%>selected="selected" <%} %>>Somalia</option>
							<option value="ZA" <%if("ZA".equals(userLogged.getCountry())){%>selected="selected" <%} %>>South Africa</option>
							<option value="GS" <%if("GS".equals(userLogged.getCountry())){%>selected="selected" <%} %>>South Georgia and the South Sandwich
								Islands</option>
							<option value="ES" <%if("ES".equals(userLogged.getCountry())){%>selected="selected" <%} %>>Spain</option>
							<option value="LK" <%if("LK".equals(userLogged.getCountry())){%>selected="selected" <%} %>>Sri Lanka</option>
							<option value="CH" <%if("CH".equals(userLogged.getCountry())){%>selected="selected" <%} %>>Switzerland</option>
							<option value="SY" <%if("SY".equals(userLogged.getCountry())){%>selected="selected" <%} %>>Syrian Arab Republic</option>
							<option value="TW" <%if("TW".equals(userLogged.getCountry())){%>selected="selected" <%} %>>Taiwan, Province of China</option>
							<option value="TJ" <%if("TJ".equals(userLogged.getCountry())){%>selected="selected" <%} %>>Tajikistan</option>
							<option value="TZ" <%if("TZ".equals(userLogged.getCountry())){%>selected="selected" <%} %>>Tanzania, United Republic of</option>
							<option value="TH" <%if("TH".equals(userLogged.getCountry())){%>selected="selected" <%} %>>Thailand</option>
							<option value="TR" <%if("TR".equals(userLogged.getCountry())){%>selected="selected" <%} %>>Turkey</option>
							<option value="TM" <%if("TM".equals(userLogged.getCountry())){%>selected="selected" <%} %>>Turkmenistan</option>
							<option value="TC" <%if("TC".equals(userLogged.getCountry())){%>selected="selected" <%} %>>Turks and Caicos Islands</option>
							<option value="UG" <%if("UG".equals(userLogged.getCountry())){%>selected="selected" <%} %>>Uganda</option>
							<option value="UA" <%if("UA".equals(userLogged.getCountry())){%>selected="selected" <%} %>>Ukraine</option>
							<option value="AE" <%if("AE".equals(userLogged.getCountry())){%>selected="selected" <%} %>>United Arab Emirates</option>
							<option value="GB" <%if("GB".equals(userLogged.getCountry())){%>selected="selected" <%} %>>United Kingdom</option>
							<option value="US" <%if("US".equals(userLogged.getCountry())){%>selected="selected" <%} %>>United States</option>
							<option value="UM" <%if("UM".equals(userLogged.getCountry())){%>selected="selected" <%} %>>United States Minor Outlying Islands</option>
							<option value="UY" <%if("UY".equals(userLogged.getCountry())){%>selected="selected" <%} %>>Uruguay</option>
							<option value="UZ" <%if("UZ".equals(userLogged.getCountry())){%>selected="selected" <%} %>>Uzbekistan</option>
							<option value="VU" <%if("VU".equals(userLogged.getCountry())){%>selected="selected" <%} %>>Vanuatu</option>
							<option value="VE" <%if("VE".equals(userLogged.getCountry())){%>selected="selected" <%} %>>Venezuela</option>
							<option value="VN" <%if("VN".equals(userLogged.getCountry())){%>selected="selected" <%} %>>Viet Nam</option>
							<option value="VG" <%if("VG".equals(userLogged.getCountry())){%>selected="selected" <%} %>>Virgin Islands (British)</option>
							<option value="VI" <%if("VI".equals(userLogged.getCountry())){%>selected="selected" <%} %>>Virgin Islands (U.S.)</option>
							<option value="WF" <%if("WF".equals(userLogged.getCountry())){%>selected="selected" <%} %>>Wallis and Futuna Islands</option>
							<option value="EH" <%if("EH".equals(userLogged.getCountry())){%>selected="selected" <%} %>>Western Sahara</option>
							<option value="YE" <%if("YE".equals(userLogged.getCountry())){%>selected="selected" <%} %>>Yemen</option>
							<option value="YU" <%if("YU".equals(userLogged.getCountry())){%>selected="selected" <%} %>>Yugoslavia</option>
							<option value="ZM" <%if("ZM".equals(userLogged.getCountry())){%>selected="selected" <%} %>>Zambia</option>
							<option value="ZW" <%if("ZW".equals(userLogged.getCountry())){%>selected="selected" <%} %>>Zimbabwe</option>
						</select>
					</div>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-4 control-label">About Yourself</label>
				<div class="col-md-6 inputGroupContainer">
					<div class="input-group">
						<span class="input-group-addon"><i
							class="glyphicon glyphicon-pencil"></i></span>
						<textarea class="form-control" id="aboutYourSelf"
							name="aboutYourSelf" rows="3" ><%=userLogged.getUserDescription()!=null?userLogged.getUserDescription():""%></textarea>
					</div>
				</div>
			</div>
			
			<div class="form-group">
					<label class="col-md-4 control-label"></label>
					<div class="col-md-4">
					<input type="hidden" name="actionType" value="updateUserProfile">
						<button type="submit" class="btn btn-warning"
							form="updateUserProfile" value="Submit">
							Save
						</button>
					</div>
				</div>
		</form>
	</div>
	<%@include file="footer.jsp"%>
</body>
</html>