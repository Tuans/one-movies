<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page
	import="java.util.List, org.bson.Document, java.util.ArrayList, one.movies.model.Movie, one.movies.model.User, one.movies.utils.Constants"%>

<%
	String loginErrorMessage = (String) request.getAttribute("loginErrorMessage");
	String registrationErrorMessage = (String) request.getAttribute("registrationErrorMessage");
	User user = (User) session.getAttribute("user");
	String currentPage = (String) session.getAttribute("currentPage");
%>
<!-- header -->
<div class="header">
	<div class="container">
		<div class="w3layouts_logo">
			<a href="${pageContext.request.contextPath}/home"><h1>
					One<span>Movies</span>
				</h1></a>
		</div>
		<div class="w3_search">
			<form action="#" method="post">
				<input type="text" name="Search" placeholder="Search" required="">
				<input type="submit" value="Go">
			</form>
		</div>
		<div class="w3l_sign_in_register">
			<ul>
				<li><i class="fa fa-phone" aria-hidden="true"></i> (+84) 123 456 789</li>
				<%
					if (user == null) {
				%>
				<li><a href="#login" data-toggle="modal" data-target="#myModal"
					id="btnPopupLogin">Login</a></li>
				<%
					} else {
				%>
				<li>
					<form action="user" method="post">
						<%if(currentPage!=null && Constants.USER_PROFILE_PAGE.equals(currentPage)) {%>
						<span class="label label-default" style="font-size: 15px"><%=user.getDisplayName()%></span>
						<%}else { %>
						<button class="btn btn-link " role="link" type="submit"
							name="actionType" value="getUserProfile"><%=user.getDisplayName()%></button>
						<%} %>
						<button class="btn btn-link" role="link" type="submit"
							name="actionType" value="logout">Logout</button>
					</form>
				</li>
				<%
					}
				%>
			</ul>
		</div>
		<div class="clearfix"></div>
	</div>
</div>
<!-- //header -->
<!-- bootstrap-pop-up -->
<div class="modal video-modal fade" id="myModal" tabindex="-1"
	role="dialog" aria-labelledby="myModal">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				Sign In & Sign Up
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<section>
				<div class="modal-body">
					<div class="w3_login_module">
						<div class="module form-module">
							<div class="toggle">
								<i class="fa fa-times fa-pencil"></i>
								<div class="tooltip">Click Me</div>
							</div>
							<div class="form">
								<h3>Login to your account</h3>
								<%
									if (loginErrorMessage != null) {
								%>
								<div class="alert alert-danger fade in">
									<a href="#" class="close" data-dismiss="alert">&times;</a> <strong>Error!</strong>
									<%=loginErrorMessage%>
								</div>
								<%
									}
								%>
								<form action="home" method="post">
									<input type="text" name="userName" placeholder="Username"
										required=""> <input type="password" name="password"
										placeholder="Password" required=""> <input
										type="submit" value="Login"> <input type="hidden"
										name="actionType" value="login">
								</form>
							</div>
							<div class="form">
								<h3>Create an account</h3>
								<%
									if (registrationErrorMessage != null) {
								%>
								<div class="alert alert-danger fade in">
									<a href="#" class="close" data-dismiss="alert">&times;</a> <strong>Error!</strong>
									<%=registrationErrorMessage%>
								</div>
								<%
									}
								%>
								<form action="#" method="post">
									<input type="text" name="userName" placeholder="Username"
										required=""> <input type="text" name="displayName"
										placeholder="Display Name" required=""> <input
										type="Password" name="password" placeholder="Password"
										required=""> <input type="email" name="email"
										placeholder="Email Address" required=""> <input
										type="text" name="phoneNumber" placeholder="Phone Number"
										required=""> <input type="submit" value="Register">
									<input type="hidden" name="actionType" value="registration">
								</form>
							</div>
							<div class="cta">
								<a href="#">Forgot your password?</a>
							</div>
						</div>
					</div>
				</div>
			</section>
		</div>
	</div>
</div>
<script>
	$('.toggle').click(function() {
		// Switches the Icon
		$(this).children('i').toggleClass('fa-pencil');
		// Switches the forms  
		$('.form').animate({
			height : "toggle",
			'padding-top' : 'toggle',
			'padding-bottom' : 'toggle',
			opacity : "toggle"
		}, "slow");
	});
</script>
<%
	if (registrationErrorMessage != null) {
%>
<script>
	$('.toggle').trigger('click');
</script>
<%
	}
%>
<!-- //bootstrap-pop-up -->
<!-- nav -->
<div class="movies_nav">
	<div class="container">
		<nav class="navbar navbar-default">
			<div class="navbar-header navbar-left">
				<button type="button" class="navbar-toggle collapsed"
					data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
			</div>
			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse navbar-right"
				id="bs-example-navbar-collapse-1">
				<nav>
					<ul class="nav navbar-nav">
						<li class="active"><a
							href="${pageContext.request.contextPath}/home">Home</a></li>
						<li class="dropdown"><a href="#" class="dropdown-toggle"
							data-toggle="dropdown">Genres <b class="caret"></b></a>
							<ul class="dropdown-menu multi-column columns-3">
								<li>
									<div class="col-sm-4">
										<ul class="multi-column-dropdown">
											<li><a href="genres.html">Action</a></li>
											<li><a href="genres.html">Biography</a></li>
											<li><a href="genres.html">Crime</a></li>
											<li><a href="genres.html">Family</a></li>
											<li><a href="horror.html">Horror</a></li>
											<li><a href="genres.html">Romance</a></li>
											<li><a href="genres.html">Sports</a></li>
											<li><a href="genres.html">War</a></li>
										</ul>
									</div>
									<div class="col-sm-4">
										<ul class="multi-column-dropdown">
											<li><a href="genres.html">Adventure</a></li>
											<li><a href="comedy.html">Comedy</a></li>
											<li><a href="genres.html">Documentary</a></li>
											<li><a href="genres.html">Fantasy</a></li>
											<li><a href="genres.html">Thriller</a></li>
										</ul>
									</div>
									<div class="col-sm-4">
										<ul class="multi-column-dropdown">
											<li><a href="genres.html">Animation</a></li>
											<li><a href="genres.html">Costume</a></li>
											<li><a href="genres.html">Drama</a></li>
											<li><a href="genres.html">History</a></li>
											<li><a href="genres.html">Musical</a></li>
											<li><a href="genres.html">Psychological</a></li>
										</ul>
									</div>
									<div class="clearfix"></div>
								</li>
							</ul></li>

						<li><a href="news.html">news</a></li>
						<li class="dropdown"><a href="#" class="dropdown-toggle"
							data-toggle="dropdown">Studio <b class="caret"></b></a>
							<ul class="dropdown-menu multi-column columns-3">
								<li>
									<div class="col-sm-4">
										<ul class="multi-column-dropdown">
											<li><a href="genres.html">Asia</a></li>
											<li><a href="genres.html">France</a></li>
											<li><a href="genres.html">Taiwan</a></li>
											<li><a href="genres.html">United States</a></li>
										</ul>
									</div>
									<div class="col-sm-4">
										<ul class="multi-column-dropdown">
											<li><a href="genres.html">China</a></li>
											<li><a href="genres.html">HongCong</a></li>
											<li><a href="genres.html">Japan</a></li>
											<li><a href="genres.html">Thailand</a></li>
										</ul>
									</div>
									<div class="col-sm-4">
										<ul class="multi-column-dropdown">
											<li><a href="genres.html">Euro</a></li>
											<li><a href="genres.html">India</a></li>
											<li><a href="genres.html">Korea</a></li>
											<li><a href="genres.html">United Kingdom</a></li>
										</ul>
									</div>
									<div class="clearfix"></div>
								</li>
							</ul></li>

					</ul>
				</nav>
			</div>
		</nav>
	</div>
</div>
<!-- //nav -->