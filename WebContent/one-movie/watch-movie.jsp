<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page
	import="java.util.List, org.bson.Document, java.util.ArrayList, one.movies.model.User, one.movies.model.Movie"%>
<!DOCTYPE html>
<html lang="en">
<head>
<title>One Movies an Entertainment Category Flat Bootstrap
	Responsive Website Template | Single :: w3layouts</title>
<!-- for-mobile-apps -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords"
	content="One Movies Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<script type="application/x-javascript">
	 addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
		function hideURLbar(){ window.scrollTo(0,1); } 
</script>
<!-- //for-mobile-apps -->
<link href="${pageContext.request.contextPath}/admin/css/bootstrap.css"
	rel="stylesheet" type="text/css" media="all" />
<link href="${pageContext.request.contextPath}/admin/css/style.css"
	rel="stylesheet" type="text/css" media="all" />
<link href="${pageContext.request.contextPath}/admin/css/medile.css"
	rel='stylesheet' type='text/css' />
<link href="${pageContext.request.contextPath}/admin/css/single.css"
	rel='stylesheet' type='text/css' />
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/admin/css/contactstyle.css"
	type="text/css" media="all" />
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/admin/css/faqstyle.css"
	type="text/css" media="all" />
<!-- news-css -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/admin/news-css/news.css"
	type="text/css" media="all" />
<!-- //news-css -->
<!-- list-css -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/admin/list-css/list.css"
	type="text/css" media="all" />
<!-- //list-css -->
<!-- font-awesome icons -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/admin/css/font-awesome.min.css" />
<!-- //font-awesome icons -->
<!-- js -->
<script type="text/javascript"
	src="${pageContext.request.contextPath}/admin/js/jquery-2.1.4.min.js"></script>
<!-- //js -->
<link
	href='//fonts.googleapis.com/css?family=Roboto+Condensed:400,700italic,700,400italic,300italic,300'
	rel='stylesheet' type='text/css'>
<!-- start-smoth-scrolling -->
<script type="text/javascript"
	src="${pageContext.request.contextPath}/admin/js/move-top.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/admin/js/easing.js"></script>

<script
	src="${pageContext.request.contextPath}/admin/js/simplePlayer.js"></script>
<script>
	$("document").ready(function() {
		$("#video").simplePlayer();
	});
</script>
<!-- Styles -->
<script>var myContextPath = "${pageContext.request.contextPath}"</script>
<link rel="stylesheet" href="${pageContext.request.contextPath}/admin/css/plyr.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/admin/css/demo.css">
<!-- player end -->
</head>
<%
	List<Movie> lstMovie = (ArrayList<Movie>) request
			.getAttribute("lstMovie");
	boolean isClickLoginPopup = (((String) request
			.getAttribute("loginErrorMessage")) != null || ((String) request
			.getAttribute("registrationErrorMessage")) != null) ? true
			: false;
	boolean isClickRegistrationTab = ((String) request
			.getAttribute("registrationErrorMessage")) != null ? true
			: false;
	Movie movie = (Movie) session.getAttribute("movie");
%>
<body>
	<!-- banner -->
	<%@include file="nav.jsp"%>
	<!-- banner -->
	<div class="general_social_icons">
		<nav class="social">
			<ul>
				<li class="w3_twitter"><a href="#">Twitter <i
						class="fa fa-twitter"></i></a></li>
				<li class="w3_facebook"><a href="#">Facebook <i
						class="fa fa-facebook"></i></a></li>
				<li class="w3_dribbble"><a href="#">Dribbble <i
						class="fa fa-dribbble"></i></a></li>
				<li class="w3_g_plus"><a href="#">Google+ <i
						class="fa fa-google-plus"></i></a></li>
			</ul>
		</nav>
	</div>
	<!-- single -->
	<div class="single-page-agile-main">
		<div class="container">
			<div class="single-page-agile-info">
				<!-- /movie-browse-agile -->
				<div class="show-top-grids-w3lagile">
				
					<div class="col-sm-8 single-left">
						<div class="song">
							<div class="song-info">
								<h3 style="font-weight: bold; "><b><%=movie!=null?movie.getMovieName():"" %></b></h3>
								<br>
								<h4><%=movie!=null?movie.getMovieDescription():"" %></h4>
							</div>
							<%if(movie!=null){ %>
							<div align="center" >
							<video poster="<%=movie.getSmallImageLink()%>">
							  <source src="<%=movie.getMovieLink()%>" type="video/mp4">
							  Your browser does not support the video tag.
							</video>
							</div>
							<%}else { %>
							<div class="video-grid-single-page-agileits">
								<img src="https://i.ytimg.com/vi/R7o1PnHas9M/maxresdefault.jpg"
									alt="" class="img-responsive" />
							</div>
							
							<%} %>
						</div>
						<div class="clearfix"></div>
<!-- Detail movie -->
						<div class="movie-information">
							<p class="movie_option">
								<strong style="font-size: 1.3em; ">Country: </strong>
								<a href="#" style="font-size: 1.1em; "><%=movie!=null?movie.getCountry():"" %></a>
							</p>
							<p class="movie_option">
								<strong style="font-size: 1.3em; ">Year: </strong>
								<a href="#" style="font-size: 1.1em; "><%=movie!=null?movie.getReleaseYear():"" %></a>
							</p>
							<p class="movie_option">
								<strong style="font-size: 1.3em; ">Genres: </strong>
								<%for(String genres : movie.ToList("Genres")) {%>
								<a href="#" style="font-size: 1.1em; "><%=genres + ", " %></a>
								<%} %>
							</p>
							<p class="movie_option">
								<strong style="font-size: 1.3em; ">Tags: </strong>
								<%for(String tags : movie.ToList("Tags")) {%>
								<a href="#" style="font-size: 1.1em; "><%=tags + ", " %></a>
								<%} %>
							</p>
							<p class="movie_option">
								<strong style="font-size: 1.3em; ">Stars: </strong>
								<%for(String stars : movie.ToList("Stars")) {%>
								<a href="#" style="font-size: 1.1em; "><%=stars %></a> ,&nbsp;
								<%} %>
							</p>

						</div>
						<!-- End Detail -->
					</div>
					
					<div class="col-md-4 single-right">
						<h3>Up Next</h3>
						<div class="single-grid-right">
							<div class="single-right-grids">
								<div class="col-md-4 single-right-grid-left">
									<a href="single.html"><img
										src="${pageContext.request.contextPath}/admin/images/m1.jpg"
										alt="" /></a>
								</div>
								<div class="col-md-8 single-right-grid-right">
									<a href="single.html" class="title"> Nullam interdum metus</a>
									<p class="author">
										<a href="#" class="author">John Maniya</a>
									</p>
									<p class="views">2,114,200 views</p>
								</div>
								<div class="clearfix"></div>
							</div>

						</div>
					</div>



					<div class="clearfix"></div>
				</div>
				<!-- //movie-browse-agile -->
				<!--body wrapper start-->
				<div class="w3_agile_banner_bottom_grid">
					<div id="owl-demo" class="owl-carousel owl-theme">
						<div class="item">
							<div class="w3l-movie-gride-agile w3l-movie-gride-agile1">
								<a href="single.html" class="hvr-shutter-out-horizontal"><img
									src="${pageContext.request.contextPath}/admin/images/m13.jpg"
									title="album-name" class="img-responsive" alt=" " />
									<div class="w3l-action-icon">
										<i class="fa fa-play-circle" aria-hidden="true"></i>
									</div> </a>
								<div class="mid-1 agileits_w3layouts_mid_1_home">
									<div class="w3l-movie-text">
										<h6>
											<a href="single.html">Citizen Soldier</a>
										</h6>
									</div>
									<div class="mid-2 agile_mid_2_home">
										<p>2016</p>
										<div class="block-stars">
											<ul class="w3l-ratings">
												<li><a href="#"><i class="fa fa-star"
														aria-hidden="true"></i></a></li>
												<li><a href="#"><i class="fa fa-star"
														aria-hidden="true"></i></a></li>
												<li><a href="#"><i class="fa fa-star"
														aria-hidden="true"></i></a></li>
												<li><a href="#"><i class="fa fa-star"
														aria-hidden="true"></i></a></li>
												<li><a href="#"><i class="fa fa-star-half-o"
														aria-hidden="true"></i></a></li>
											</ul>
										</div>
										<div class="clearfix"></div>
									</div>
								</div>
								<div class="ribben">
									<p>NEW</p>
								</div>
							</div>
						</div>

					</div>
				</div>
				<!--body wrapper end-->

			</div>
			<!-- //w3l-latest-movies-grids -->
		</div>
	</div>
	<!-- //w3l-medile-movies-grids -->
	<script src="${pageContext.request.contextPath}/admin/js/plyr.js"></script>

	<!-- Docs script -->
	<script src="${pageContext.request.contextPath}/admin/js/demo.js"></script>

	<%@include file="footer.jsp"%>
</body>
</html>