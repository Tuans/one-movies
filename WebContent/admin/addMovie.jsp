<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Add Movie</title>
<script src="http://s.codepen.io/assets/libs/modernizr.js"
	type="text/javascript"></script>
<link rel='stylesheet prefetch'
	href='http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css'>
<link rel='stylesheet prefetch'
	href='http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap-theme.min.css'>
<link rel='stylesheet prefetch'
	href='http://cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.0/css/bootstrapValidator.min.css'>
<link rel="stylesheet" href="${pageContext.request.contextPath}/admin/css/style_admin.css">
</head>
<body>
	<%
		String message = (String) request.getAttribute("Message");
		String errMessage = (String) request.getAttribute("ErrorMessage");
	%>
	<div class="container">
		<div class="jumbotron">
			<h1>Admin Page</h1>
			<p></p>
		</div>
	</div>
	<div class="container">
		<form class="well form-horizontal" action="#" method="post"
			id="insertMovieForm">
			<fieldset>
				<!-- Form Name -->
				<legend>Insert a Film</legend>
				<%
					if (message != null) {
				%>
				<div class="bs-example">
					<div class="alert alert-success fade in">
						<a href="#" class="close" data-dismiss="alert">&times;</a> <strong>Success!</strong>
						<%=message%>
					</div>
				</div>
				<%
					}
				%>
				<!-- Text input-->
				<div class="form-group">
					<label class="col-md-4 control-label">Movie Name</label>
					<div class="col-md-4 inputGroupContainer">
						<div class="input-group">
							<span class="input-group-addon"><i
								class="glyphicon glyphicon-user"></i></span> <input name="movieName"
								placeholder="Movie Name" class="form-control" type="text">
						</div>
					</div>
					<%
						if (errMessage != null) {
					%>
					<div class="col-md-4 alert alert-danger fade in">
						<a href="#" class="close" data-dismiss="alert">&times;</a> <strong>Error!</strong>
						<%=errMessage%>
					</div>
					<%
						}
					%>
				</div>
				<!-- Text input-->
				<div class="form-group">
					<label class="col-md-4 control-label">Director</label>
					<div class="col-md-4 inputGroupContainer">
						<div class="input-group">
							<span class="input-group-addon"><i
								class="glyphicon glyphicon-user"></i></span> <input name="director"
								placeholder="Director" class="form-control" type="text">
						</div>
					</div>
				</div>
				<!-- Text input-->
				<div class="form-group">
					<label class="col-md-4 control-label">Studio</label>
					<div class="col-md-4 inputGroupContainer">
						<div class="input-group">
							<span class="input-group-addon"><i
								class="glyphicon glyphicon-home"></i></span> <input name="studio"
								placeholder="Studio" class="form-control" type="text">
						</div>
					</div>
				</div>
				<!-- Text input-->
				<div class="form-group">
					<label class="col-md-4 control-label">Country</label>
					<div class="col-md-4 inputGroupContainer">
						<div class="input-group">
							<span class="input-group-addon"><i
								class="glyphicon glyphicon-home"></i></span> <input name="country"
								placeholder="Country" class="form-control" type="text">
						</div>
					</div>
				</div>
				<!-- Text input-->
				<div class="form-group">
					<label class="col-md-4 control-label">Genres</label>
					<div class="col-md-4 inputGroupContainer">
						<div class="input-group">
							<span class="input-group-addon"><i
								class="glyphicon glyphicon-search"></i></span> <input name="genres"
								placeholder="Genres" class="form-control" type="text" value="21+">
						</div>
					</div>
				</div>
				<!-- Text input-->
				<div class="form-group">
					<label class="col-md-4 control-label">Tags</label>
					<div class="col-md-4 inputGroupContainer">
						<div class="input-group">
							<span class="input-group-addon"><i
								class="glyphicon glyphicon-globe"></i></span> <input name="tags"
								placeholder="Tags" class="form-control" type="text">
						</div>
					</div>
				</div>
				<!-- Text input-->
				<div class="form-group">
					<label class="col-md-4 control-label">Stars</label>
					<div class="col-md-4 inputGroupContainer">
						<div class="input-group">
							<span class="input-group-addon"><i
								class="glyphicon glyphicon-user"></i></span> <input name="stars"
								placeholder="Stars" class="form-control" type="text">
						</div>
					</div>
				</div>
				<!-- Text input-->
				<div class="form-group">
					<label class="col-md-4 control-label">Old Site</label>
					<div class="col-md-4 inputGroupContainer">
						<div class="input-group">
							<span class="input-group-addon"><i
								class="glyphicon glyphicon-link"></i></span> <input name="oldSite"
								placeholder="Ols Site" class="form-control" type="text">
						</div>
					</div>
				</div>
				<!-- Text input-->
				<div class="form-group">
					<label class="col-md-4 control-label">Movie Link</label>
					<div class="col-md-4 inputGroupContainer">
						<div class="input-group">
							<span class="input-group-addon"><i
								class="glyphicon glyphicon-link"></i></span> <input name="movieLink"
								placeholder="Movie Link" class="form-control" type="text">
						</div>
					</div>
				</div>
				<!-- Text input-->
				<div class="form-group">
					<label class="col-md-4 control-label">Large Image Link</label>
					<div class="col-md-4 inputGroupContainer">
						<div class="input-group">
							<span class="input-group-addon"><i
								class="glyphicon glyphicon-link"></i></span> <input name="largeImageLink"
								placeholder="Large Image Link" class="form-control" type="text">
						</div>
					</div>
				</div>
				<!-- Text input-->
				<div class="form-group">
					<label class="col-md-4 control-label">Small Image Link</label>
					<div class="col-md-4 inputGroupContainer">
						<div class="input-group">
							<span class="input-group-addon"><i
								class="glyphicon glyphicon-link"></i></span> <input name="smallImageLink"
								placeholder="Small Image Link" class="form-control" type="text">
						</div>
					</div>
				</div>
				<!-- Select Basic -->

				<div class="form-group">
					<label class="col-md-4 control-label">ReleaseYear</label>
					<div class="col-md-4 selectContainer">
						<div class="input-group">
							<span class="input-group-addon"><i
								class="glyphicon glyphicon-list"></i></span> <select name="releaseYear"
								class="form-control selectpicker">
								<option value=" ">Please select a Year</option>
								<option>2010</option>
								<option>2011</option>
								<option>2012</option>
								<option>2013</option>
								<option>2014</option>
								<option>2015</option>
								<option>2016</option>
								<option selected="selected">2017</option>
							</select>
						</div>
					</div>
				</div>

				<!-- Text area -->
				<div class="form-group">
					<label class="col-md-4 control-label">Movie Description</label>
					<div class="col-md-6 inputGroupContainer">
						<div class="input-group">
							<span class="input-group-addon"><i
								class="glyphicon glyphicon-pencil"></i></span>
							<textarea class="form-control" name="movieDescription"
								placeholder="Movie Description"></textarea>
						</div>
					</div>
				</div>
				<!-- Success message -->
				<!-- <div class="alert alert-success" role="alert" id="success_message">
					Success <i class="glyphicon glyphicon-thumbs-up"></i> Thanks for
					contacting us, we will get back to you shortly.
				</div> -->
				<!-- Button -->
				<div class="form-group">
					<label class="col-md-4 control-label"></label>
					<div class="col-md-4">
						<button type="submit" class="btn btn-warning"
							form="insertMovieForm" value="Submit">
							Submit <span class="glyphicon glyphicon-send"></span>
						</button>
					</div>
				</div>
			</fieldset>
		</form>
	</div>

	<!-- /.container -->
	<script
		src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
	<script
		src='http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js'></script>
	<script
		src='http://cdnjs.cloudflare.com/ajax/libs/bootstrap-validator/0.4.5/js/bootstrapvalidator.min.js'></script>

	<script src="${pageContext.request.contextPath}/admin/js/index.js"></script>

</body>
</html>
